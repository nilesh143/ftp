--select [dbo].[get_EmplList](273)
CREATE   FUNCTION [dbo].[get_EmplList](@rateeList varchar(8000), @cycle_sequence_id int,@question_id int,@emplid varchar(10))        
returns  varchar(8000)        
as        
begin        
   DECLARE @getScaleID CURSOR  
 declare @user_name varchar(100)     
 declare @rateeid varchar(100)     
 declare @raterid varchar(100)     
  declare @user_display_name varchar(100)    
  declare @scalelist   varchar(5000)    
  set @scalelist=''
  declare @qrating int
   SELECT  @raterid=user_id FROM  [framework_mstr_user_general_details]where  [user_name]=@emplid
  and is_active='Y'
  
    SET @getScaleID = CURSOR       
    FOR SELECT  
       user_id
      , [user_name]
      ,[user_display_name]       
  FROM  [framework_mstr_user_general_details] a ,  [dbo].[Splitid](@rateeList,'|')b
  where a.[user_name]=b.id and a.is_active='Y'
       
    OPEN @getScaleID       
       
    FETCH next FROM @getScaleID INTO  @rateeid,@user_name,@user_display_name
       
    WHILE @@FETCH_STATUS = 0       
      BEGIN       
     
    if exists( SELECT [question_id] FROM  [encompass_mpng_question_role]
  where [is_active]='Y' and [question_id]=@question_id
   and role_id=(select role_id from [encompass_mpng_cycle_sequence_ratee_rater] where [cycle_sequence_id]=@cycle_sequence_id and [ratee_id]=@rateeid and [rater_id]=@raterid and is_active='Y')  )
	begin
	set @qrating=0
      SELECT   
      @qrating=[question_impt_rating]     
  FROM  [encompass_trxn_feedback]
  where  [ratee_id]=@rateeid
      and [rater_id]=@raterid
      and [cycle_sequence_id]=@cycle_sequence_id
      
       set @scalelist=@scalelist+@user_name+':' + @user_display_name+'#'+convert(varchar(2),@qrating)+'|' 
     end
     
      FETCH next FROM @getScaleID INTO  @rateeid,@user_name,@user_display_name    
               
      END       
       
    CLOSE @getScaleID       
       
    DEALLOCATE @getScaleID 

  
 --print @scalelist
 set @scalelist=substring(@scalelist,0,len(@scalelist))
return   @scalelist     
end  


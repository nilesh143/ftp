--[saveFeedbackQuestionare] '139092','SAVE',10135,'2852#250582:12$231797:7$A1004:4|2854#250582:7$231797:4|3#A1002:4$A1003:3$A1004:5'

CREATE PROCEDURE [dbo].[saveFeedbackQuestionare] @emplid varchar(10), @action varchar(10) ,@cycleid int,@questionRatings text
AS 
begin
 
--[saveFeedbackQuestionare] '250472','SAVE',74,'777#147621:5$231797:1$A1004:4|778#147621:6$231797:1|3#A1002:4$A1003:3$A1004:5'

SET NOCOUNT  ON
--separators meaning
--| question separator
--# question resposne separator
--: employee -rating separator
--$ employee separator
declare @statuscode varchar(4)
declare @message varchar(2000)
set @statuscode='200'
set @message='Feedback submitted successfully'
declare @cycle_id int
 
declare @ratingList varchar(8000)
set @ratingList='1#A1002:2$A1003:1$A1004:4|2#A1002:5$A1003:3$A1004:2|3#A1002:4$A1003:3$A1004:5'
declare @questionnaire_id int
SELECT  @questionnaire_id=[questionnaire_id] FROM  [encompass_mstr_cycle] where [is_active]='Y' and [cycle_id]=@cycleid
  
declare @raterid int
declare @rateeid int
  --select substring(ID,0,CHARINDEX('#',ID)) QID, substring(ID, CHARINDEX('#',ID)+1  ,len(ID)) response from dbo.splitid(@questionRatings,'|')  
   SELECT  @raterid=user_id FROM  [framework_mstr_user_general_details]where  [user_name]=@emplid
  and is_active='Y'
 print @raterid
  declare @cycle_sequence_id int
 
 select @cycle_sequence_id=   [mpng_cycle_sequence_id]  FROM  [encompass_mpng_cycle_sequence] where cycle_id=@cycleid
  print '@cycle_sequence_id'
   print @cycle_sequence_id
  CREATE TABLE #RATER_FEEDBACK_TEMP(
	[EMPLID] [varchar](10) NULL,
	[CYCLEID] [int] NULL,
	[QUESTION_ID] [int] NULL,
	[RATEEID] [varchar](10) NULL,
	[RATING] varchar(10) NULL,
	[STATUS] [varchar](200) NULL,
	Role_ID int ,
)  

  
   DECLARE @getRatings CURSOR  
 declare @qid varchar(100)     
  declare @ratings varchar(8000)    
  --declare @rateeid   varchar(10)  
    declare @rating   varchar(10)   
   
    set @rating=''
    SET @getRatings = CURSOR       
    FOR   select substring(ID,0,CHARINDEX('#',ID)) qid, substring(ID, CHARINDEX('#',ID)+1  ,len(ID)) ratings from dbo.splitid(@questionRatings,'|')  
       
    OPEN @getRatings       
       
    FETCH next FROM @getRatings INTO @qid,@ratings
       
    WHILE @@FETCH_STATUS = 0       
      BEGIN       
      INSERT INTO #RATER_FEEDBACK_TEMP select @emplid emplid,  @cycleid cycleid,@qid qid,  substring(ID,0,CHARINDEX(':',ID))   ,substring(ID, CHARINDEX(':',ID)+1  ,len(ID)),'' ,
      (select role_id from [encompass_mpng_cycle_sequence_ratee_rater] where [cycle_sequence_id]=@cycle_sequence_id and [ratee_id]=( SELECT   user_id FROM  [framework_mstr_user_general_details]where  [user_name]= substring(ID,0,CHARINDEX(':',ID))   and is_active='Y' ) and [rater_id]=@raterid and is_active='Y') role_id  
      
      from dbo.splitid(@ratings,'$')
  --select role_id from [encompass_mpng_cycle_sequence_ratee_rater] where [cycle_sequence_id]=1032  and [ratee_id]=( SELECT   user_id FROM  [framework_mstr_user_general_details]where  [user_name]= 'A1002'  and is_active='Y' ) and [rater_id]=5 and is_active='Y'
       
      FETCH next FROM @getRatings INTO @qid,@ratings   
               
      END       
       
    CLOSE @getRatings       
       
    DEALLOCATE @getRatings 
    
    -- validaitons 
--encompass_trxn_feedback - to store feedback
--encompass_mpng_cycle_sequence - to check cycle status 
--encompass_mpng_cycle_ratee - to update ratee status
--encompass_mpng_cycle_sequence_ratee_rater - to update rater status
--Encompass_mstr_quorum - for quorum update
-- save to encompass_trxn_feedback
--if exists(SELECT     encompass_trxn_feedback_1.trxn_feedback_id
--FROM         encompass_trxn_feedback INNER JOIN
--                      encompass_trxn_feedback AS encompass_trxn_feedback_1 ON encompass_trxn_feedback.trxn_feedback_id = encompass_trxn_feedback_1.trxn_feedback_id AND 
--                      encompass_trxn_feedback.ratee_id = encompass_trxn_feedback_1.ratee_id AND encompass_trxn_feedback.rater_id = encompass_trxn_feedback_1.rater_id AND 
--                      encompass_trxn_feedback.cycle_sequence_id = encompass_trxn_feedback_1.cycle_sequence_id AND 
--                      encompass_trxn_feedback.question_id = encompass_trxn_feedback_1.question_id AND encompass_trxn_feedback.role_id = encompass_trxn_feedback_1.role_id   )
--begin
              
        print 'updating  encompass_trxn_feedback......'             
UPDATE  [encompass_trxn_feedback]
   SET 
      [question_impt_rating] = rating
      
      ,[is_completed] = case when len(rating)>=1 then 'Y' else 'N' end 
      ,[mod_time] =getdate()
      ,[mod_id] =  @raterid
FROM         encompass_trxn_feedback INNER JOIN
                      #RATER_FEEDBACK_TEMP AS encompass_trxn_feedback_1 ON  
                      encompass_trxn_feedback.ratee_id =  (SELECT   user_id FROM  [framework_mstr_user_general_details]where  [user_name]= RATEEID   and is_active='Y' )AND encompass_trxn_feedback.rater_id = @raterid AND 
                      encompass_trxn_feedback.cycle_sequence_id = @cycle_sequence_id AND 
                      encompass_trxn_feedback.question_id = encompass_trxn_feedback_1.question_id
AND 
 encompass_trxn_feedback.[role_id] = encompass_trxn_feedback_1.[role_id]

 --WHERE [ratee_id] = <ratee_id, bigint,>
 --     and [rater_id] = <rater_id, varchar(50),>
 --     and [role_id] = <role_id, bigint,>
 --     and [cycle_sequence_id] = <cycle_sequence_id, bigint,>
 --     and [question_id] =
   
 
 print 'Inserting  encompass_trxn_feedback......'  
INSERT INTO [encompass_trxn_feedback]
           ([ratee_id]
           ,[rater_id]
           ,[role_id]
           ,[cycle_sequence_id]
           ,[question_id]
           ,[question_comment]
           ,[question_impt_rating]
           ,[question_freq_rating]
           ,[question_expt_rating]
           ,[is_completed]
         --  ,[mod_time]
        --   ,[mod_id]
           ,[cre_time]
           ,[cre_id])
           select 

           (SELECT   user_id FROM  [framework_mstr_user_general_details]where  [user_name]= RATEEID   and is_active='Y' ) RATEEID
           ,@raterid raterid
           ,role_id 
           ,@cycle_sequence_id cycle_sequence_id
           ,question_id
           ,''
           ,rating
           ,''
           ,''
           ,case when len(rating)>=1 then 'Y' else 'N' end 
            
           ,getdate()
           , @raterid
           from #RATER_FEEDBACK_TEMP where  role_id is not  null 
           AND (CONVERT(VARCHAR(8),(SELECT   user_id FROM  [framework_mstr_user_general_details]where  [user_name]= RATEEID   and is_active='Y' ) )+
           CONVERT(VARCHAR(8),@raterid )+
           CONVERT(VARCHAR(8),role_id )+
           CONVERT(VARCHAR(8),@cycle_sequence_id )+
           CONVERT(VARCHAR(8),question_id ) 
          
           NOT IN(
           
           SELECT CONVERT(VARCHAR(8),[ratee_id])+
           CONVERT(VARCHAR(8),[rater_id] )+
           CONVERT(VARCHAR(8),role_id )+
           CONVERT(VARCHAR(8),cycle_sequence_id )+
           CONVERT(VARCHAR(8),question_id)  FROM [encompass_trxn_feedback] where cycle_sequence_id=@cycle_sequence_id
           ))
 

declare @feedback_status varchar(2)

declare @selffeedback_status varchar(2)
--Update status for  self feedback  
if exists(select rateeid from #RATER_FEEDBACK_TEMP where emplid=rateeid and emplid=@emplid)
begin
declare @rateestatus varchar(2)
declare @selfQuestionCount int
declare @selfQuestionCountAns int
set @selffeedback_status='L'
SELECT @selfQuestionCount=count([question_id])
        FROM  [encompass_mpng_question_role] where [is_active]='Y' and [questionnaire_id]=@questionnaire_id and [role_id]=( SELECT  [ROLE_ID]       FROM  [encompass_mstr_role] where   upper([ROLE_NAME]) ='SELF' and[IS_ACTIVE]='Y' and [QUESTIONNAIRE_ID]=@questionnaire_id)
        
  select @selfQuestionCountAns=count(rateeid) from #RATER_FEEDBACK_TEMP where emplid=rateeid and emplid=@emplid and len(rating)>=1
  
 set @selffeedback_status= case when @selfQuestionCountAns=@selfQuestionCount then 'C' else 'L' end
 
 --------------------------
 --update status for others
 --update on ratee rater status encompass_mpng_cycle_sequence_ratee_rater
 declare @rateeid_fb varchar(10)
 declare @roleid_fb int
 declare @otherCompleteCount int

    DECLARE @updateRateeeRaterStatus CURSOR  
    
    set @otherCompleteCount=0
  SET @updateRateeeRaterStatus  = CURSOR       
    FOR   select distinct rateeid,role_id   from #RATER_FEEDBACK_TEMP
       
    OPEN @updateRateeeRaterStatus       
       
    FETCH next FROM @updateRateeeRaterStatus INTO @rateeid_fb,@roleid_fb
       
    WHILE @@FETCH_STATUS = 0       
      BEGIN   
      set @feedback_status='L'
     
declare @otherQuestionCount int
declare @otherQuestionCountAns int
SELECT @otherQuestionCount=count([question_id])
        FROM  [encompass_mpng_question_role] where [is_active]='Y' and [questionnaire_id]=@questionnaire_id and [role_id]= @roleid_fb
        
  select @otherQuestionCountAns=count(rateeid) from #RATER_FEEDBACK_TEMP where  emplid=@emplid and
  rateeid=@rateeid_fb and  len(rating)>=1
  
 set @feedback_status= case when @otherQuestionCountAns=@otherQuestionCount then 'C' else 'L' end
 set @otherCompleteCount= (case   @feedback_status  when 'C' then 1 else 0 end)
 print 'updating  [encompass_mpng_cycle_sequence_ratee_rater]......'  
 UPDATE  [encompass_mpng_cycle_sequence_ratee_rater]
   SET --[mpng_cycle_ratee_id] = <mpng_cycle_ratee_id, bigint,>
       [feedback_status] = @feedback_status
      ,[mod_time] =getdate()
      ,[mod_id] = @raterid
      ,[FEEDBACK_COUNT] = @otherQuestionCountAns
      
 WHERE [is_active]='Y' and [cycle_sequence_id]=@cycle_sequence_id and [ratee_id]=( SELECT   user_id FROM  [framework_mstr_user_general_details]where  [user_name]=@rateeid_fb  and is_active='Y') and [rater_id]=@raterid and [role_id]=@roleid_fb
 
  print 'updating  [encompass_mstr_quorum]'
 UPDATE [encompass_mstr_quorum]
   SET  [completed_no] = [completed_no]+(   case  @feedback_status  when 'C' then 1 else 0 end)
      ,[mod_time] = getdate()
      ,[mod_id] = @raterid 
      
 WHERE [is_active]='Y' and [ratee_id]=( SELECT   user_id FROM  [framework_mstr_user_general_details]where  [user_name]=@rateeid_fb  and is_active='Y') and [cycle_sequence_id]=@cycle_sequence_id and [role_id]= @roleid_fb
 
   print 'updating  [encompass_mpng_cycle_ratee]'
 UPDATE [encompass_mpng_cycle_ratee]
	   SET [feedback_status] = @selffeedback_status
		  ,[mod_time] = getdate()
		  ,[mod_id] =  @raterid
		  ,[FEEDBACK_COUNT] =  [FEEDBACK_COUNT]+  @otherCompleteCount     
	 WHERE  ratee_id= @rateeid_fb and cycle_sequence_id =@cycle_sequence_id and is_active='Y'
	 
 FETCH next FROM @updateRateeeRaterStatus INTO  @rateeid_fb,@roleid_fb
               
      END       
       
    CLOSE @getRatings       
       
    DEALLOCATE @getRatings 


	
 
 end
 
    if upper(@action)='SUBMIT' 
    begin
  
    update #RATER_FEEDBACK_TEMP set status=case when rating ='' then 'Error: Please select rating for all questions and submit' else ''end 
      if exists(select '' from #RATER_FEEDBACK_TEMP where status like 'Error%')
      begin
      set @message='Error : Please select rating for all questions and submit'
      set @statuscode='500'
      end
      else
      begin
      print '--'
      end
      
      
    end
    if upper(@action)='SAVE' 
    begin
    set @message='Feedback saved successfully'
      set @statuscode='200'
    end
    
   --SELECT * FROM #RATER_FEEDBACK_TEMP
   If(OBJECT_ID('tempdb..#RATER_FEEDBACK_TEMP') Is Not Null)
Begin
--print 'Table exisits'
     Drop Table #RATER_FEEDBACK_TEMP
End

   select @statuscode statusCode,@message  message
end

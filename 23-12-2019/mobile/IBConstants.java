 package com.engage.commons;

public class IBConstants {
	
	
	
	
	
	//Tfeedback
	public static String SRV_NM_RATEE_LIST = "SRV_NM_RATEE_LIST"; // Service Pending approval details
//	public static String T_RATER_ID = "T_RATER_ID"; // Service Pending approval details
	
	public static String T_RATEE_ID = "T_RATEE_ID"; 
	public static String T_RATEE_NAME = "T_RATEE_NAME"; 
	public static String T_RATER_ID = "T_RATER_ID"; 
	public static String T_RATER_NAME = "T_RATER_NAME"; 

	public static String T_CYCLE_START_DATE = "T_CYCLE_START_DATE";
	public static String T_CYCLE_END_DATE = "T_CYCLE_END_DATE";
	
	public static String T_CYCLE_NAME = "T_CYCLE_NAME"; 
	public static String T_CYCLE_DESC = "T_CYCLE_DESC"; 
	public static String T_CYCLE_ID= "T_CYCLE_ID"; 
	
	public static String T_FEEDBACK_COUNT= "T_FEEDBACK_COUNT";
	public static String T_FEEDBACK_STATUS= "T_FEEDBACK_STATUS";
	
	public static String T_ROLE_NAME= "T_ROLE_NAME";
	
	public static String T_USER_DEPARTMENT= "T_USER_DEPARTMENT";
	
	
	
	
	
	public static String T_RATEE_COUNT= "T_RATEE_COUNT"; 
	
	
	
	//queswtionare response...
	
	public static String T_CYCLE_SEQ_ID = "T_CYCLE_SEQ_ID"; 
	public static String T_QUESTION_ID = "T_QUESTION_ID"; 
	public static String T_COMPETENCY_ID = "T_COMPETENCY_ID"; 
	public static String T_QSTNARE_ID = "T_QSTNARE_ID"; 
	public static String T_QSTN_TEXT = "T_QSTN_TEXT"; 
	public static String T_QSTN_TYPE = "T_QSTN_TYPE"; 
	public static String T_QSTN_IMPT_SCALE_ID= "T_QSTN_IMPT_SCALE_ID"; 
	
	public static String T_QSTN_FREQ_SCALE_ID= "T_QSTN_FREQ_SCALE_ID"; 
	
	public static String T_QSTN_EXPT_SCALE_ID= "T_QSTN_EXPT_SCALE_ID"; 
	
	
	public static String T_QSTN_ORDER = "T_QSTN_ORDER"; 
	public static String T_QSTNARE_NAME = "T_QSTNARE_NAME"; 
	public static String T_QSTNARE_DESC = "T_QSTNARE_DESC"; 
	public static String T_FREQ_SCALE = "T_FREQ_SCALE"; 

	public static String T_IMPT_SCALE="T_IMPT_SCALE";
	
    public static String T_EMP_LIST= "T_EMP_LIST"; 
	
	
	
	
    public static String T_FDBK_STS_CODE= "T_FDBK_STS_CODE"; 
    public static String T_FDBK_STS_MSG= "T_FDBK_STS_MSG"; 
	
	
	


	
	
	

	
	// REQ-> Request
	// RSP->Response
	public static String ADT_RSTR_CD_ENAGE = "E"; // Audit TYP code
	public static String ADT_RSTR_CD_PSFT = "P"; // Audit TYP code
	public static String ADT_CD_LGN = "SGN_IN"; // Audit TYP code
	public static String ADT_CD_LOGIN = "LOG_IN"; // Audit TYP code
	public static String ADT_CD_LGN_REG = "SGN_IN_REG"; // Audit Registration code
	public static String ADT_CD_LGN_REG_OTP = "SGN_IN_REG_OTP"; // Audit OTP code
	public static String ADT_CD_LGN_REG_MPIN = "SGN_IN_REG_MPIN"; // Audit MPIN code
	public static String ADT_CD_LGN_AUTO = "SGN_IN_AUTO"; // Audit Auto login code
	public static String ADT_CD_LGT = "SGN_OT"; // Audit TYP code
	public static String ADT_CD_NTFN_REG = "NTFN_REG"; // Audit TYP code
	public static String ADT_CD_SAL_SLP = "SL_SLP"; // Audit TYP code
	
	// PF, Gratuity, Superannuation
	public static String ADT_CD_MY_FIN_PF = "MY_FIN_PF";
	public static String ADT_CD_MY_FIN_GRAT = "MY_FIN_GRAT";
	public static String ADT_CD_MY_FIN_SUP = "MY_FIN_SUP";
	public static String ADT_CD_VW_NTFN = "VW_NTFN";
	public static String ADT_CD_MS_UPL = "MS_UPL";
	public static String ADT_CD_MS_APY = "MS_APY";
	public static String ADT_CD_MS_HST = "MS_HST";
	public static String ADT_CD_MS_PEN = "MS_PEN";
	public static String ADT_CD_MS_PND = "MS_PND";
	public static String ADT_CD_MS_APR = "MS_APR";
	public static String ADT_CD_LV_APY = "LV_APY";
	public static String ADT_CD_LV_HST = "LV_HST";
	public static String ADT_CD_LV_HSD = "LV_HSD";
	public static String ADT_CD_LV_PEN = "LV_PEN";
	public static String ADT_CD_LV_PND = "LV_PND";
	public static String ADT_CD_LV_APR = "LV_APR";
	public static String ADT_CD_LV_BAL = "LV_BAL";
	public static String ADT_CD_LV_TCL = "LV_TCL";
	public static String ADT_CD_EXP_HST = "EXP_HST";
	public static String ADT_CD_EXP_HSD = "EXP_HSD";
	public static String ADT_CD_EXP_DEL = "EXP_DEL";
	public static String ADT_CD_EXP_APY = "EXP_APY";
	public static String ADT_CD_EXP_PNA = "EXP_PNA";
	public static String ADT_CD_EXP_PND = "EXP_PND";
	public static String ADT_CD_EXP_APR = "EXP_APR";
	public static String ADT_CD_EXP_SBMT = "EXP_SBMT";
	public static String ADT_CD_OFR_HOME_LOC = "OFR_HOME_LOC";
	public static String ADT_CD_OFR_HOME_CITY = "OFR_HOME_CITY";
	public static String ADT_CD_OFR_HOME_LST = "OFR_HOME_LST";
	public static String ADT_CD_OFR_HOME_DETS = "OFR_HOME_DETS";
	public static String ADT_CD_OFR_CAR_BRND = "OFR_CAR_BRND";
	public static String ADT_CD_OFR_CAR_LST = "OFR_CAR_LST";
	public static String ADT_CD_OFR_CAR_DETS = "OFR_CAR_DETS";
	public static String ADT_CD_OFR_OTHR_LST = "OFR_OTHR_LST";
	public static String ADT_CD_OFR_OTHR_DETS = "OFR_OTHR_DETS";
	public static String ADT_CD_PER_DETS = "PER_DETS";
	public static String ADT_CD_PTH_LST = "PTH_LST";
	public static String ADT_CD_TEST_CAR_BRND = "TEST_CAR_BRND"; // Test controller
	public static String ADT_CD_PTRL_APY = "PTRL_APY";
	public static String ADT_CD_PTRL_SBMT = "PTRL_SBMT";
	public static String ADT_CD_COUNTRY = "COUNTRY";
	public static String ADT_CD_PER_DETS_UPDT = "PER_DETS_UPDT";
	public static String ADT_CD_ATM_LST = "ATM_LST";
	public static String ADT_CD_BRCH_LST = "BRCH_LST";
	public static String ADT_CD_UNLK_ID = "UNLK_ID";
	public static String ADT_CD_LV_MULT_APR = "LV_MULT_APR";
	public static String ADT_CD_MS_MULT_APR = "MS_MULT_APR";
	public static String ADT_CD_LV_P_CON = "LV_P_CON";
	public static String ADT_CD_MS_P_CON = "MS_P_CON";
	public static String ADT_CD_EXP_P_CON = "EXP_P_CON";
	public static String ADT_CD_LP_P_CON = "LP_P_CON";
	public static String ADT_CD_OFR_INTR = "OFR_INTR";
	public static String ADT_CD_LP_PEN = "LATE_PNCH_LST";
	public static String ADT_CD_LP_DETS = "LATE_PNCH_DETS";
	public static String ADT_CD_LP_APR = "LATE_PNCH_APR";
	public static String ADT_CD_LP_MULT_APR = "LATE_PNCH_MULT_APR";
	public static String ADT_CD_LP_UPL = "LATE_PNCH_UPL";
	public static String ADT_CD_LP_HIST = "LATE_PNCH_HIST";
	public static String ADT_CD_LP_SBMT = "LATE_PNCH_SBMT";
	public static String ADT_CD_MY_TM = "MY_TM";
	public static String ADT_CD_BTH_RMND = "BTH_RMND";
	public static String ADT_CD_EMG_HELP = "EMG_HELP";
	public static String ADT_CD_RESET_MPIN = "RESET_MPIN";
	public static String ADT_CD_DE_REG = "DE_REG";
	public static String ADT_CD_MNU_MPIN = "_MNU_MPIN";
	public static String ADT_CD_REG_LST = "REG_LST";
	public static String ADT_CD_ESOP_DET = "ESOP_DET";
	public static String ADT_CD_MISC_PROF = "MISC_PROF";
	public static String ADT_CD_MISC_PROF_UPDT = "MISC_PROF_UPDT";
	public static String ADT_CD_NTFN = "NTFN";
	public static String ADT_CD_NTFN_LST = "NTFN_LST";
	public static String ADT_CD_NTFN_FIVE = "NTFN_FIVE";
	public static String ADT_CD_THRD_PRTY_TOKEN = "THRD_PRTY_TOKEN";
	public static String ADT_CD_THRD_PRTY_VRFY = "THRD_PRTY_VRFY";
	public static String ADT_CD_FEED_SUB = "FEED_SUB";
	public static String ADT_CD_VIDEO_LIKE_SUB = "VIDEO_LIKE_SUB";
	public static String ADT_CD_AADHAR_SUB = "AADHAR_SUB";
	public static String ADT_CD_ISAFE_SUB = "ISAFE_SUB";
	public static String ADT_CD_INV_80CC_ULD = "INV_80CC_ULD";
	public static String ADT_CD_INV_LTA_ULD = "INV_LTA_ULD";
	public static String ADT_CD_INV_RENT_ULD = "INV_RENT_ULD";
	public static String ADT_CD_INV_OTH_ULD = "INV_OTH_ULD";
	public static String ADT_CD_INV_ADDTL_ULD = "INV_ADDTL_ULD";
	public static String ADT_CD_INV_TLPNE_ULD = "INV_TLPNE_ULD";
	public static String ADT_CD_INV_LOS_HSG_ULD = "INV_LOS_HSG_ULD";
	public static String ADT_CD_INV_PTRL_ULD = "INV_PTRL_ULD";
	public static String ADT_CD_INV_MDCL_ULD = "INV_MDCL_ULD";
	public static String ADT_CD_INV_UPL_TYP = "INV_UPL_TYP";
	public static String ADT_CD_INV_DEL_DATA = "INV_DEL_DATA";
	public static String ADT_CD_INV_VIEW_DATA = "INV_VIEW_DATA";
	public static String ADT_CD_INV_LOSS_HOUSING_SBMT = "INV_LOSS_HOUSING_SBMT";
	public static String ADT_CD_INV_LTA_INSERT = "INV_LTA_INSERT";
	public static String ADT_CD_INV_INVST_SUMMARY = "INV_INVST_SUMMARY";
	public static String ADT_CD_INV_INVST_FAQ = "INV_INVST_FAQ";
	public static String ADT_CD_INV_INVST_SUMMARY_UPDATE = "INV_INVST_SUMMARY_UPDATE";
	public static String ADT_CD_INV_INVST_LENDER_UPDATE = "INV_INVST_LENDER_UPDATE";

	public static String ADT_CD_TALK_2_HR_NTFN = "TALK_2_HR_NTFN"; // talk 2 hr
	public static String ADT_CD_ICICI_VIDEOS = "ICICI_VIDEOS"; // talk 2 hr
	public static String ADT_CD_AWARD_DETL = "AWARD_DETL"; // DNA Award details 
	public static String ADT_CD_AWARD_DESC = "AWARD_DESC"; // DNA Award description
 	public static String ADT_CD_AWARD_EVENT = "AWARD_EVENT"; // DNA Award Event details
	public static String ADT_CD_INV_DEL_LENDER = "INV_DEL_LENDER";
	public static String ADT_CD_BROWSER_VIDEO = "BROWSER_VIDEO";

	// investment declaration
	public static String ADT_CD_INV_DECL_TXT_DETL = "TXT_DETL";
	public static String ADT_CD_INV_DECL_OTH_DETL = "OTH_DETL";
	public static String ADT_CD_INV_DECL_LTA_DETL = "LTA_DETL";
	public static String ADT_CD_INV_DECL_RNT_DETL = "RNT_DETL";
	public static String ADT_CD_INV_DECL_LNDLORD_DETL = "LNDLORD_DETL";
	public static String ADT_CD_INV_DECL_NPS_DETL = "NPS_DETL";
	public static String ADT_CD_INV_DECL_VPF_DETL = "VPF_DETL";
	public static String ADT_CD_INV_DECL_LOAN_DETL = "LOAN_DETAILS";
	public static String ADT_CD_INV_DECL_EMP_SUBMIT = "EMP_SUBMIT";
	public static String ADT_CD_INV_DECL_PAN_UPDATE = "PAN_UPDATE";
	public static String ADT_CD_INV_DECL_LTA_UPDATE = "LTA_UPDATE";
	public static String ADT_CD_INV_DECL_VPF_UPDATE = "VPF_UPDATE";
	public static String ADT_CD_INV_DECL_NPS_UPDATE = "NPS_UPDATE";
	public static String ADT_CD_INV_DECL_LANDLORD_UPDATE = "LANDLORD_UPDATE";
	public static String ADT_CD_INV_DECL_RENT_UPDATE = "RENT_UPDATE";
	public static String ADT_CD_INV_DECL_TAX_DEDT = "TAX_DEDUCTION";
	public static String ADT_CD_INV_DECL_PAN_DETL = "PAN_DETL";
	public static String ADT_CD_INV_DECL_TAX_DETL = "TAX_DEDUCTION";
	public static String ADT_CD_INV_DECL_OTHR_DETL = "OTHR_DETL";
	
	//Tms Audit codes
	public static String ADT_CD_BSC_RATING = "BSC_RATING";
	public static String ADT_CD_TMS_FEEDBACK_PENDING = "FEEDBACK_PENDING";
	public static String ADT_CD_TMS_FEEDBACK_FORM = "FEEDBACK_FORM";
	public static String ADT_CD_TMS_FEEDBACK_DATA = "FEEDBACK_DATA";
	public static String ADT_CD_TMS_DRAFT_FEEDBACK_DATA = "DRAFT_FEEDBACK_DATA";
    public static String ADT_CD_TMS_FEEDBACK_AGENDA_DTLS = "FEEDBACK AGENDA DETIALS";
    public static String ADT_CD_TMS_FEEDBACK_UPDATE = "FEEDBACK_UPDATE";  
    public static String ADT_CD_TMS_EXCHANGE_STS = "EXCHANGE_STS";
    public static String ADT_CD_EBOD_SUBMIT = "EBOD_SUBMIT";
    
    // EGES AUDIT CODES
    public static String ADT_CD_EGES_JOB_LIST = "EGES_JOB_LIST";
    public static String ADT_CD_EGES_JOB_DESC = "EGES_JOB_DESC";
    public static String ADT_CD_EGES_UPD_RESUME = "EGES_UPD_RESUME";
    public static String ADT_CD_EGES_SMT_RESUME = "EGES_SMT_RESUME";
    
    //APP FEEDBACK SUBMIT
    public static String ADT_CD_APP_FEEDBACK_SUBMIT = "APP_FEEDBACK_SUBMIT";
    
    //NEES Audit Code
    public static String ADT_CD_NEES_GET_DETAILS = "NEES_GET_DETAILS";
    
    
    public static String ADT_CD_ICE_TEAM_CONTACTS = "ICE_TEAM_CONTACTS";
    
    public static String ADT_CD_FUN_LV_APY = "FUN_LV_APY";
    
   //My day  events
    public static String ADT_CD_SUBSCRIBE_EVENT = "ADT_CD_SUBSCRIBE_EVENT";
    public static String ADT_CD_GET_EVENTS = "ADT_CD_GET_EVENTS";
    public static String ADT_CD_GET_EVENTS_EDT_LST = "ADT_CD_GET_EVENTS_EDT_LST";
    
    //My day  quick links
    public static String ADT_CD_QUICKLINK_SET_ORDER = "ADT_CD_QUICKLINK_SET_ORDER";
    public static String ADT_CD_GET_QUICKLINKS = "ADT_CD_GET_QUICKLINKS";
    public static String ADT_CD_GET_QUICKLINK_EDT_LST = "ADT_CD_GET_QUICKLINK_EDT_LST";
    
    
    //My day  other apps
    public static String ADT_CD_OTHERAPPS_SET_ORDER = "ADT_CD_OTHERAPPS_SET_ORDER";
   
 
	//Investment Declaration Services
		public static String SRV_NM_INVST_DECL_GET_TX_DTLS = "SRV_NM_INVST_DECL_GET_TX_DTLS"; // Service GetTaxDetails
		public static String SRV_NM_INVST_DECL_GET_OTH_DTLS = "SRV_NM_INVST_DECL_GET_OTH_DTLS"; // Service GetOtherDetails
		public static String SRV_NM_INVST_DECL_GET_LTA_DTLS = "SRV_NM_INVST_DECL_GET_LTA_DTLS"; // Service GetLTADetails
		public static String SRV_NM_INVST_DECL_GET_VPF_DTLS = "SRV_NM_INVST_DECL_GET_VPF_DTLS"; // Service GetVPFDetails
		public static String SRV_NM_INVST_DECL_GET_RNT_DTLS = "SRV_NM_INVST_DECL_GET_RNT_DTLS"; // Service GetRentDetails
		public static String SRV_NM_INVST_DECL_GET_TX_DEDTCN = "SRV_NM_INVST_DECL_GET_TX_DEDTCN"; // Service GetTaxDeduction
		public static String SRV_NM_INVST_DECL_GET_INV_NPS_DTLS = "SRV_NM_INVST_DECL_GET_INV_NPS_DTLS"; // Service GetInvNPSDetails
		public static String SRV_NM_INVST_DECL_GET_CM_LNDRD_DTLS = "SRV_NM_INVST_DECL_GET_CM_LNDRD_DTLS"; // Service GetCMLandlordDetails
		public static String SRV_NM_INVST_DECL_GET_PAN_DTLS = "SRV_NM_INVST_DECL_GET_PAN_DTLS";// Service GetPanDetails
		public static String SRV_NM_INVST_DECL_LOAN_DTLS = "SRV_NM_INVST_DECL_LOAN_DTLS"; //Service InvLoanDetails
		public static String SRV_NM_INVST_DECL_EMP_INV_SBT = "SRV_NM_INVST_DECL_EMP_INV_SBT"; //Service InvSubmit
		public static String SRV_NM_INVST_DECL_UPDT_PAN_DTLS = "SRV_NM_INVST_DECL_UPDT_PAN_DTLS"; //Service UpdatePanDeatils
		public static String SRV_NM_INVST_DECL_UPDT_LTA_DTLS = "SRV_NM_INVST_DECL_UPDT_LTA_DTLS"; //Service UpdateLTADetails
		public static String SRV_NM_INVST_DECL_UPDT_VPF_DTLS = "SRV_NM_INVST_DECL_UPDT_VPF_DTLS"; //Service UpdateVPFDetails
		public static String SRV_NM_INVST_DECL_UPDT_NPS_DTLS = "SRV_NM_INVST_DECL_UPDT_NPS_DTLS"; //Service UpdateNPSDetails
		public static String SRV_NM_INVST_DECL_UPDT_LNDLRD_DTLS	= "SRV_NM_INVST_DECL_UPDT_LNDLRD_DTLS"; //Service UpdateLandlordDetails
		public static String SRV_NM_INVST_DECL_UPDT_RNT_DTLS = "SRV_NM_INVST_DECL_UPDT_RNT_DTLS"; //Service UpdateRentDetails
		public static String SRV_NM_INVST_DECL_UPDT_TX_DDCTN = "SRV_NM_INVST_DECL_UPDT_TX_DDCTN"; //Service UpdateTaxDeduction
		public static String SRV_NM_INVST_DECL_UPDT_TX_DTLS	= "SRV_NM_INVST_DECL_UPDT_TX_DTLS"; //Service UpdateTaxDetails
		public static String SRV_NM_INVST_DECL_UPDT_OTHR_DTLS	= "SRV_NM_INVST_DECL_UPDT_OTHR_DTLS"; //Service UpdateOtherDetails
		public static String SRV_NM_INVST_DECL_UPDT_EXCHANGE_STS	= "SRV_NM_INVST_DECL_UPDT_EXCHANGE_STS"; //Service UpdateExchanegStatus
		
		
	//InvestmentDeclaration Request Individual Constants
		public static String INVST_DECL_REQ_EMPL_ID = "INVST_DECL_REQ_EMPL_ID"; //Employee No
		public static String INVST_DECL_REQ_PAN_NO = "INVST_DECL_REQ_PAN_NO"; //PAN Number
		public static String INVST_DECL_REQ_ADDR = "INVST_DECL_REQ_ADDR"; //Address
		public static String INVST_DECL_REQ_EMPL_NM = "INVST_DECL_REQ_EMPL_NM"; // Employee Name
		public static String INVST_DECL_REQ_AIR_AMT = "INVST_DECL_REQ_AIR_AMT"; // Air Amount
		public static String INVST_DECL_REQ_RAIL_AMT = "INVST_DECL_REQ_RAIL_AMT"; // Rail Amount
		public static String INVST_DECL_REQ_ROAD_AMT = "INVST_DECL_REQ_ROAD_AMT"; // Road Amount
		public static String INVST_DECL_REQ_TOTAL_AMT = "INVST_DECL_REQ_TOTAL_AMT"; // Total Amount
		public static String INVST_DECL_REQ_VPF_AMT = "INVST_DECL_REQ_VPF_AMT"; // VPF Amount
		public static String INVST_DECL_REQ_VPF_PERC = "INVST_DECL_REQ_VPF_PERC"; // VPF PERC
		public static String INVST_DECL_REQ_FRM_DT = "INVST_DECL_REQ_FRM_DT"; // From Date
		public static String INVST_DECL_REQ_TO_DT = "INVST_DECL_REQ_TO_DT"; // To Date
		public static String INVST_DECL_REQ_NPS_PR = "INVST_DECL_REQ_NPS_PR"; // NPS PR
		public static String INVST_DECL_REQ_PAN_LST = "INVST_DECL_REQ_PAN_LST"; // Pan List0
		public static String INVST_DECL_REQ_ADDR_LST = "INVST_DECL_REQ_ADDR_LST"; // Address List
		public static String INVST_DECL_REQ_LND_LRD_NM_LST = "INVST_DECL_REQ_LND_LRD_NM_LST"; // LandLord Name List
		public static String INVST_DECL_REQ_LND_LRD_ADDR_LST = "INVST_DECL_REQ_LND_LRD_ADDR_LST"; // LandLord Address List
		public static String INVST_DECL_REQ_USR_ID = "INVST_DECL_REQ_USR_ID"; // User Id
		public static String INVST_DECL_REQ_RNT_AMT = "INVST_DECL_REQ_RNT_AMT"; // Rent Amount
		public static String INVST_DECL_REQ_RNT_PRD = "INVST_DECL_REQ_RNT_PRD"; // Rent Period
		public static String INVST_DECL_REQ_TX_INCM = "INVST_DECL_REQ_TX_INCM"; // Tax Income
		public static String INVST_DECL_REQ_TX_OTHR_INCM = "INVST_DECL_REQ_TX_OTHR_INCM"; // Tax Other Income
		public static String INVST_DECL_REQ_TX_CAP_GAIN = "INVST_DECL_REQ_TX_CAP_GAIN"; // Tax Cap Gain
		public static String INVST_DECL_REQ_TX_DDCTN = "INVST_DECL_REQ_TX_DDCTN"; // Tax Deduction
		public static String INVST_DECL_REQ_TX_SHRT_CD = "INVST_DECL_REQ_TX_SHRT_CD"; // Tax Short Code
		public static String INVST_DECL_REQ_INVST_AMT = "INVST_DECL_REQ_INVST_AMT"; // Investment Amount
		
		public static String INVST_DECL_REQ_INVST_STS = "INVST_DECL_REQ_INVST_AMT"; // Investment STATUS
		
		//Investment Declaration Request List Constants for multiple Update
		public static String INVST_DECL_REQ_LST_UPDT_PAN_DTLS = "INVST_DECL_REQ_LST_UPDT_PAN_DTLS"; // UpdatePanDetails List
		public static String INVST_DECL_REQ_LST_UPDT_LTA_DTLS = "INVST_DECL_REQ_LST_UPDT_LTA_DTLS"; // UpdateLTADetails List
		public static String INVST_DECL_REQ_LST_UPDT_VPF_DTLS = "INVST_DECL_REQ_LST_UPDT_VPF_DTLS"; // UpdateVPFDetails List
		public static String INVST_DECL_REQ_LST_UPDT_NPS_DTLS = "INVST_DECL_REQ_LST_UPDT_NPS_DTLS"; // UpdateNPSDetails List
		public static String INVST_DECL_REQ_LST_UPDT_LNDLRD_DTLS = "INVST_DECL_REQ_LST_UPDT_LNDLRD_DTLS"; // UpdateLandLordDetails List
		public static String INVST_DECL_REQ_LST_UPDT_RNT_DTLS = "INVST_DECL_REQ_LST_UPDT_RNT_DTLS"; // UpdateRentDetails List
		public static String INVST_DECL_REQ_LST_UPDT_TX_DDCTN = "INVST_DECL_REQ_LST_UPDT_TX_DDCTN"; // UpdateTaxDeduction List
		public static String INVST_DECL_REQ_LST_UPDT_TX_DTLS = "INVST_DECL_REQ_LST_UPDT_TX_DTLS"; // UpdateTaxDetails List
		public static String INVST_DECL_REQ_LST_UPDT_OTHR_DTLS = "INVST_DECL_REQ_LST_UPDT_OTHR_DTLS"; // UpdateOtherDetails List
		
		// Investment Declaration Response Constants
		public static String INVST_DECL_RES_T05_INV_ITEM = "INVST_DECL_RES_T05_INV_ITEM";
		public static String INVST_DECL_RES_T05_INV_ITEM_DESC = "INVST_DECL_RES_T05_INV_ITEM_DESC";
		public static String INVST_DECL_RES_T05_SEX = "INVST_DECL_RES_T05_SEX";
		public static String INVST_DECL_RES_T04_INV_FLAG = "INVST_DECL_RES_T04_INV_FLAG";
		public static String INVST_DECL_RES_T04_INV_GROUP = "INVST_DECL_RES_T04_INV_GROUP";
		public static String INVST_DECL_RES_T05_LIMIT = "INVST_DECL_RES_T05_LIMIT";
		public static String INVST_DECL_RES_T05_DELETE_TAG = "INVST_DECL_RES_T05_DELETE_TAG";
		public static String INVST_DECL_RES_USER_ID = "INVST_DECL_RES_USER_ID";
		public static String INVST_DECL_RES_MDFD_DTE = "INVST_DECL_RES_MDFD_DTE";
		public static String INVST_DECL_RES_TIME_STAMP = "INVST_DECL_RES_TIME_STAMP";
		public static String INVST_DECL_RES_T06_INV_AMOUNT = "INVST_DECL_RES_T06_INV_AMOUNT";
		public static String INVST_DECL_RES_SR_NO = "INVST_DECL_RES_SR_NO";
		public static String INVST_DECL_RES_T01_TAX_CAL_CODE = "INVST_DECL_RES_T01_TAX_CAL_CODE";
		public static String INVST_DECL_RES_H01_EMP_NUM = "INVST_DECL_RES_H01_EMP_NUM";
		public static String INVST_DECL_RES_EMP_NAME = "INVST_DECL_RES_EMP_NAME";
		public static String INVST_DECL_RES_T06_INV_PERC = "INVST_DECL_RES_T06_INV_PERC";
		public static String INVST_DECL_RES_VPF_FROM_DT = "INVST_DECL_RES_VPF_FROM_DT";
		public static String INVST_DECL_RES_VPF_TO_DT = "INVST_DECL_RES_VPF_TO_DT";
		public static String INVST_DECL_RES_INV_PERC = "INVST_DECL_RES_INV_PERC";
		public static String INVST_DECL_RES_FRM_MNT = "INVST_DECL_RES_FRM_MNT";
		public static String INVST_DECL_RES_FRM_YR = "INVST_DECL_RES_FRM_YR";
		public static String INVST_DECL_RES_TO_MNT = "INVST_DECL_RES_TO_MNT";
		public static String INVST_DECL_RES_TO_YR = "INVST_DECL_RES_TO_YR";
		public static String INVST_DECL_RES_AIR_AMT = "INVST_DECL_RES_AIR_AMT";
		public static String INVST_DECL_RES_RAIL_AMT = "INVST_DECL_RES_RAIL_AMT";
		public static String INVST_DECL_RES_ROAD_AMT = "INVST_DECL_RES_ROAD_AMT"; 
		public static String INVST_DECL_RES_TOTAL_AMT = "INVST_DECL_RES_TOTAL_AMT";
		public static String INVST_DECL_RES_PRD = "INVST_DECL_RES_PRD";
		public static String INVST_DECL_RES_TAX_CODE = "INVST_DECL_RES_TAX_CODE";
		public static String INVST_DECL_RES_CAP_GAIN = "INVST_DECL_RES_CAP_GAIN";
		public static String INVST_DECL_RES_OTH_INCOME = "INVST_DECL_RES_OTH_INCOME";
		public static String INVST_DECL_RES_INCOME = "INVST_DECL_RES_INCOME";
		public static String INVST_DECL_RES_DEDCTN = "INVST_DECL_RES_DEDCTN";
		public static String INVST_DECL_RES_YR_CODE = "INVST_DECL_RES_YR_CODE";
		public static String INVST_DECL_RES_NPS_FROM_DT = "INVST_DECL_RES_NPS_FROM_DT";
		public static String INVST_DECL_RES_NPS_TO_DT = "INVST_DECL_RES_NPS_TO_DT";
		public static String INVST_DECL_RES_ID = "INVST_DECL_RES_ID";
		public static String INVST_DECL_RES_TAX_YEAR = "INVST_DECL_RES_TAX_YEAR";
		public static String INVST_DECL_RES_PAN_NO = "INVST_DECL_RES_PAN_NO";
		public static String INVST_DECL_RES_ADDR = "INVST_DECL_RES_ADDR";
		public static String INVST_DECL_RES_STATE = "INVST_DECL_RES_STATE";
		public static String INVST_DECL_RES_CITY = "INVST_DECL_RES_CITY";
		public static String INVST_DECL_RES_PIN_CD = "INVST_DECL_RES_PIN_CD";
		public static String INVST_DECL_RES_CRTD_DT = "INVST_DECL_RES_CRTD_DT";
		public static String INVST_DECL_RES_CRTD_BY = "INVST_DECL_RES_CRTD_BY";
		public static String INVST_DECL_RES_UPDTD_DT = "INVST_DECL_RES_UPDTD_DT";
		public static String INVST_DECL_RES_UPDTD_BY = "INVST_DECL_RES_UPDTD_BY";
		public static String INVST_DECL_RES_STS = "INVST_DECL_RES_STS";
		public static String INVST_DECL_RES_LDRD_NM = "INVST_DECL_RES_LDRD_NM";
		public static String INVST_DECL_RES_LDRD_ADDR = "INVST_DECL_RES_LDRD_ADDR";
		public static String INVST_DECL_RES_EMPL_NO = "INVST_DECL_RES_EMPL_NO"; // Employee Number
		public static String INVST_DECL_RES_NAME = "INVST_DECL_RES_NAME"; // Name
		public static String INVST_DECL_RES_LOAN_CD = "INVST_DECL_RES_LOAN_CD"; // Loan Code
		public static String INVST_DECL_RES_ORDR_NO = "INVST_DECL_RES_ORDR_NO"; // Order Number
		public static String INVST_DECL_RES_CNST_N_CMPLT = "INVST_DECL_RES_CNST_N_CMPLT";
		public static String INVST_DECL_RES_EMPL_ID = "INVST_DECL_RES_EMPL_ID"; // Employee ID
		public static String INVST_DECL_RES_IS_SBMT = "INVST_DECL_RES_IS_SBMT"; // Is Submit
	
	/* 
	 * TMS FeedBack Services 
	 * */
	public static String SRV_NM_TMS_FDBK_ADD_EMP_CMPTNCY_DTLS = "SRV_NM_TMS_FDBK_ADD_EMP_CMPTNCY_DTLS"; //Service AddEmployeeCompetencyDetails
	public static String SRV_NM_TMS_FDBK_GET_EMP_CMPTNCY = "SRV_NM_TMS_FDBK_GET_EMP_CMPTNCY"; // Service GetEmployeeCompetency
	public static String SRV_NM_TMS_FDBK_GET_SCT_PNDG_FDBK = "SRV_NM_TMS_FDBK_GET_SCT_PNDG_FDBK"; //Service GetScoutPendingFeedback
	public static String SRV_NM_TMS_FDBK_GET_CMPTNCY_BY_FDBK_DTL_ID = "SRV_NM_TMS_FDBK_GET_CMPTNCY_BY_FDBK_DTL_ID"; //Service Get DraftCompetency By FeedbackDetailId
	public static String SRV_NM_TMS_FDBK_UPDT_FDBK_AGND_DTLS = "SRV_NM_TMS_FDBK_UPDT_FDBK_AGND_DTLS"; // Service to UpdateFeedback Agenda Details
	public static String SRV_NM_TMS_FDBK_UPDT_EXCHANGE_STATUS = "SRV_NM_TMS_FDBK_UPDT_EXCHANGE_STATUS"; // Service to Update Exchange Status
	public static String SRV_NM_TMS_FDBK_UPDT = "SRV_NM_TMS_FDBK_UPDT"; // Service to Update Feedback 
	
	
	/* 
	 * TMS Feedback Request Constants 
	 * */
	public static String TMS_FDBK_REQ_USR_ID = "TMS_FDBK_REQ_USR_ID"; // Employee Id
	public static String TMS_FDBK_REQ_SCID = "TMS_FDBK_REQ_SCID"; // 
	public static String TMS_FDBK_REQ_USR_TS_MPNG_ID = "TMS_FDBK_REQ_USR_TS_MPNG_ID"; // UserTsMapping Id
	public static String TMS_FDBK_REQ_FDBK_STS = "TMS_FDBK_REQ_FDBK_STS"; // Feedback Status
	public static String TMS_FDBK_REQ_LGD_USR_ID = "TMS_FDBK_REQ_LGD_USR_ID"; // Logged in User Id
	public static String TMS_FDBK_REQ_FDBK_AGND = "TMS_FDBK_REQ_FDBK_AGND"; // Feedback Agenda
	public static String TMS_FDBK_REQ_FDBK_DT = "TMS_FDBK_REQ_FDBK_DT"; // Feedback Date
	public static String TMS_FDBK_REQ_FDBK_DESC = "TMS_FDBK_REQ_FDBK_DESC"; // Feedback Description
	public static String TMS_FDBK_REQ_FDBK_RTNG = "TMS_FDBK_REQ_FDBK_RTNG"; // Feedback Rating
	public static String TMS_FDBK_REQ_FDBK_DTL_ID = "TMS_FDBK_REQ_FDBK_DTL_ID"; // Feedback Detail Id
	public static String TMS_FDBK_REQ_CMPTNCY_ID = "TMS_FDBK_REQ_CMPTNCY_ID"; // Competency Id
	public static String TMS_FDBK_REQ_CMPTNCY_TRAIT_ID = "TMS_FDBK_REQ_CMPTNCY_TRAIT_ID"; // Competency Trait Id
	public static String TMS_FDBK_REQ_TRAIT_RTNG = "TMS_FDBK_REQ_TRAIT_RTNG"; // Trait Rating
	public static String TMS_FDBK_REQ_TRAIT_DESC = "TMS_FDBK_REQ_TRAIT_DESC"; 
	public static String TMS_FDBK_REQ_CMPTNCY_NAME = "TMS_FDBK_REQ_CMPTNCY_NAME"; 
	public static String TMS_FDBK_REQ_CMPTNCY_IS_MANDATORY = "TMS_FDBK_REQ_CMPTNCY_IS_MANDATORY"; 
	public static String TMS_FDBK_REQ_TRAIT_IS_MANDATORY = "TMS_FDBK_REQ_TRAIT_IS_MANDATORY";
	public static String TMS_FDBK_REQ_MNGR_ID = "TMS_FDBK_REQ_MNGR_ID"; // Manager Id
	public static String TMS_FDBK_REQ_PRD = "TMS_FDBK_REQ_PRD"; // Period
	public static String TMS_FDBK_REQ_STS = "TMS_FDBK_REQ_STS"; // Status
	public static String TMS_FDBK_REQ_MEETING_ID = "TMS_FDBK_REQ_MEETING_ID"; // Meeting id
		
	/*
	 * TMS Feedback List Request Constants 
	 * */ 
	public static String TMS_FDBK_REQ_CMPTNCY_LST = "TMS_FDBK_REQ_CMPTNCY_LST"; // Competency List
	public static String TMS_FDBK_REQ_TRAIT_LST = "TMS_FDBK_REQ_TRAIT_LST"; // Trait List
	
	/* 
	 * TMS Feedback Response Constants
	 *  */
	public static String TMS_FDBK_RES_CMPTNCY_ID = "TMS_FDBK_RES_CMPTNCY_ID"; // Competency Id
	public static String TMS_FDBK_RES_CMPTNCY_NM = "TMS_FDBK_RES_CMPTNCY_NM"; // Competency Name
	public static String TMS_FDBK_RES_CMPTNCY_CLR = "TMS_FDBK_RES_CMPTNCY_CLR"; // Competency Color
	public static String TMS_FDBK_RES_IS_MNDTY = "TMS_FDBK_RES_IS_MNDTY"; // Is Manadatory
	public static String TMS_FDBK_RES_TRIT_DESC = "TMS_FDBK_RES_TRIT_DESC";// Trait Description
	public static String TMS_FDBK_RES_CMPTNCY_TRITS_ID = "TMS_FDBK_RES_CMPTNCY_TRITS_ID"; // Competency Trait Id
	public static String TMS_FDBK_RES_TRIT_CLR = "TMS_FDBK_RES_TRIT_CLR"; // Trait Color
	public static String TMS_FDBK_RES_CMPTNCY_MSTR_IS_ACTV = "TMS_FDBK_RES_CMPTNCY_MSTR_IS_ACTV"; // Competenecy Master Is Active 
	public static String TMS_FDBK_RES_CMPTNCY_TRIT_IS_ACTV = "TMS_FDBK_RES_CMPTNCY_TRIT_IS_ACTV"; // Comptenecy Trait Is Active
	public static String TMS_FDBK_RES_IS_RTD = "TMS_FDBK_RES_IS_RTD"; // Is Rated
	public static String TMS_FDBK_RES_RTNG = "TMS_FDBK_RES_RTNG"; // Rating
	public static String TMS_FDBK_RES_FDBK_DTL_ID = "TMS_FDBK_RES_FDBK_DTL_ID"; // Feedback Detail Id
	public static String TMS_FDBK_RES_TRIT_IS_MNDTY = "TMS_FDBK_RES_TRIT_IS_MNDTY"; // Trait Is Mandatory
	public static String TMS_FDBK_RES_FDBK_RTNG = "TMS_FDBK_RES_FDBK_RTNG"; // Feedback Rating
	public static String TMS_FDBK_RES_FDBK_AGND = "TMS_FDBK_RES_FDBK_AGND"; // Feedback Agenda
	public static String TMS_FDBK_RES_FDBK_RTNG_ID = "TMS_FDBK_RES_FDBK_RTNG_ID"; // Feedback Rating Id
	public static String TMS_FDBK_RES_FDBK_DT = "TMS_FDBK_RES_FDBK_DT"; // Feedback Date
	public static String TMS_FDBK_RES_FDBK_DESC = "TMS_FDBK_RES_FDBK_DESC"; //Feedback Description
	public static String TMS_FDBK_RES_FDBK_STS = "TMS_FDBK_RES_FDBK_STS"; // Feedback Status
	public static String TMS_FDBK_RES_EMPL_ID = "TMS_FDBK_RES_EMPL_ID"; // Employee Id
	public static String TMS_FDBK_RES_EMPL_NM = "TMS_FDBK_RES_EMPL_NM"; // Employee Name
	public static String TMS_FDBK_RES_ICI_SAL_SHRT_DESCR = "TMS_FDBK_RES_ICI_SAL_SHRT_DESCR"; // ICI_SAL_SHRT_DESCR
	public static String TMS_FDBK_RES_USR_TS_MPNG_ID = "TMS_FDBK_RES_USR_TS_MPNG_ID"; // UserTsMapping Id
	public static String TMS_FDBK_RES_DEPT = "TMS_FDBK_RES_DEPT"; // Department
	public static String TMS_FDBK_RES_PRF_URL = "TMS_FDBK_RES_PRF_URL"; // Profile URL
	public static String TMS_FDBK_RES_GRD = "TMS_FDBK_RES_GRD"; // Grade
	public static String TMS_FDBK_RES_RPTNG_TO_ID = "TMS_FDBK_RES_RPTNG_TO_ID"; // Reporting To ID
	public static String TMS_FDBK_RES_IS_MNGR = "TMS_FDBK_RES_IS_MNGR"; // Is Manager
	public static String TMS_FDBK_RES_STS = "TMS_FDBK_RES_STS"; // Is status
	
	public static String TMS_FDBK_RES_MEETING_ID = "TMS_FDBK_RES_MEETING_ID"; // Meeting id
	public static String TMS_FDBK_RES_MEETING_SUB = "TMS_FDBK_RES_MEETING_SUB"; // Meeting subject
	public static String TMS_FDBK_RES_MEETING_ST_DT = "TMS_FDBK_RES_MEETING_ST_DT"; // Meeting start date
	public static String TMS_FDBK_RES_MEETING_ED_DT = "TMS_FDBK_RES_MEETING_ED_DT"; // end date
	
	
	/*
	 * public static String ADT_CD_LGT ="SGN_OT"; //Audit TYP code public static
	 * String ADT_CD_LGT ="SGN_OT"; //Audit TYP code public static String
	 * ADT_CD_LGT ="SGN_OT"; //Audit TYP code public static String ADT_CD_LGT
	 * ="SGN_OT"; //Audit TYP code
	 */
	/**
	 * Service Req Parameters
	 */
	public static String SRV_NM_NTFN_REG = "SRV_NM_NTFN_REG"; // Service Name Notification Registration
	public static String SRV_NM_ADT = "SRV_NM_ADT"; // Service Name Audit only
	public static String SRV_NM_LGN = "SRV_NM_LGN"; // Service Name Login
	public static String SRV_NM_LGN_REG = "SRV_NM_LGN_REG"; // Service people-soft login Registration
	public static String SRV_NM_LGN_REG_OTP = "SRV_NM_LGN_REG_OTP"; // Service people-soft login Registration OTP
	public static String SRV_NM_LGN_REG_MPIN = "SRV_NM_LGN_REG_MPIN"; // Service people-soft login Registration Mpin

	// public static String SRV_NM_LGN_LDAP = "SRV_NM_LGN_LDAP"; // Service LDAP
	// login
	public static String SRV_NM_LGN_AUTO = "SRV_NM_LGN_AUTO"; // Service auto login
	public static String SRV_NM_LGT = "SRV_NM_LGT"; // Service Name Logout
	public static String SRV_NM_SL_SLP = "SRV_NM_SL_SLP"; // Service Salary Slip
	public static String SRV_NM_LV_APY = "SRV_NM_LV_APY"; // Service Leave apply
	public static String SRV_NM_LV_HST = "SRV_NM_LV_HST"; // Service leave history
	public static String SRV_NM_LV_HSD = "SRV_NM_LV_HSD"; // Service leave history details
	public static String SRV_NM_LV_BAL = "SRV_NM_LV_BAL"; // Service Leave balance
	public static String SRV_NM_LV_BAL_CNT = "SRV_NM_LV_BAL_CNT"; // Service Leave Balance Count
	public static String SRV_NM_LV_PEN = "SRV_NM_LV_PEN"; // Service Pending approval list
	public static String SRV_NM_LV_PND = "SRV_NM_LV_PND"; // Service Pending approval details
	public static String SRV_NM_LV_APR = "SRV_NM_LV_APR"; // Service Leave approve / reject
	public static String SRV_NM_LV_TCL = "SRV_NM_LV_TCL"; // Service Team Calendar
	public static String SRV_NM_MS_ULT = "SRV_NM_MS_ULT"; // Service Muster Update List / Days empty
	public static String SRV_NM_MS_APY = "SRV_NM_MS_APY"; // Service Muster apply
	public static String SRV_NM_MS_HST = "SRV_NM_MS_HST"; // Service Muster history list
	public static String SRV_NM_MS_PEN = "SRV_NM_MS_PEN"; // Service Muster
															// pending list
	public static String SRV_NM_MS_PND = "SRV_NM_MS_PND"; // Service muster
															// pending list
															// details
	public static String SRV_NM_MS_APR = "SRV_NM_MS_APR"; // Service Muster
															// approve / reject
	public static String SRV_NM_EXP_HIST = "SRV_NM_EXP_HIST"; // Service expense
																// history
	public static String SRV_NM_EXP_DET = "SRV_NM_EXP_DET"; // Service Expense
															// History details
	public static String SRV_NM_EXP_DEL = "SRV_NM_EXP_DEL"; // Service Expense
															// History details
															// Delete
	public static String SRV_NM_EXP_PRPSBMT = "SRV_NM_EXP_PRPSBMT"; // Service
																	// Expense
																	// Apply
																	// form
	public static String SRV_NM_EXP_SBMT = "SRV_NM_EXP_SBMT"; // Service Expense
																// Apply form
																// submit
	public static String SRV_NM_EXP_PND = "SRV_NM_EXP_PND"; // Service Expense
															// Pending Approvals
															// list
	public static String SRV_NM_EXP_PND_DET = "SRV_NM_EXP_PND_DET"; // Service
																	// Expense
																	// Pending
																	// Approvals
																	// Details
	public static String SRV_NM_EXP_APP_REJ = "SRV_NM_EXP_APP_REJ"; // Service
																	// Expense
																	// Pending
																	// Approvals
																	// - Approve
																	// Reject
	public static String SRV_NM_OFR_HOME_LOC = "SRV_NM_OFR_HOME_LOC"; // Service
																		// Home
																		// Offer
																		// Location
	public static String SRV_NM_OFR_HOME_LOCN = "SRV_NM_OFR_HOME_LOCN"; // Service
																		// Home
																		// Offer
																		// Location
																		// NEW
	public static String SRV_NM_OFR_HOME_LOC_CITY = "SRV_NM_OFR_HOME_LOC_CITY"; // Service
																				// Home
																				// Offer
																				// Location
																				// City
	public static String SRV_NM_OFR_HOME_LST = "SRV_NM_OFR_HOME_LST"; // Service
																		// Home
																		// Offer
																		// List
	public static String SRV_NM_OFR_HOMES_LST = "SRV_NM_OFR_HOMES_LST"; // Service
																		// Home
																		// Offer
																		// List
	public static String SRV_NM_OFR_HOME_OFF = "SRV_NM_OFR_HOME_OFF"; // Service
																		// Home
																		// Offer
																		// List
	public static String SRV_NM_OFR_HOME_DETS = "SRV_NM_OFR_HOME_DETS"; // Service
																		// Home
																		// Offer
																		// Details
	public static String SRV_NM_OFR_CAR_BRND = "SRV_NM_OFR_CAR_BRND"; // Service
																		// Car
																		// Offer
																		// Brand
	public static String SRV_NM_OFR_CAR_LST = "SRV_NM_OFR_CAR_LST"; // Service
																	// Car Offer
																	// List
	public static String SRV_NM_OFR_CAR_DETS = "SRV_NM_OFR_CAR_DETS"; // Service
																		// Car
																		// Offer
																		// Details
	public static String SRV_NM_OFR_OTHR_LST = "SRV_NM_OFR_OTHR_LST"; // Service
																		// Other
																		// Offer
																		// List
	public static String SRV_NM_OFR_OTHR_DETS = "SRV_NM_OFR_OTHR_DETS"; // Service
																		// Other
																		// Offer
																		// Details
	public static String SRV_NM_TEST_CAR_BRND = "SRV_NM_TEST_CAR_BRND"; // Service
																		// Test
																		// Offer
																		// Brand
	public static String SRV_NM_PER_DETS = "SRV_NM_PER_DETS"; // Service
																// Personal
																// Details
	public static String SRV_NM_PER_DETS_UPDT = "SRV_NM_PER_DETS_UPDT"; // Service
																		// Personal
																		// Details
																		// Update
	public static String SRV_NM_HOME_DETS = "SRV_NM_HOME_DETS";
	public static String SRV_NM_HOME_LST = "SRV_NM_HOME_DETS";
	public static String SRV_NM_LC_PND_CON = "SRV_NM_LC_PND_CON"; // Service
																	// Personal
																	// Details
	public static String SRV_NM_MS_PND_CONT = "SRV_NM_MS_PND_CON"; // Service
																	// Personal
																	// Details
	public static String SRV_NM_EXP_PND_CON = "SRV_NM_EXP_PND_CON"; // Service
																	// Personal
																	// Details
	public static String SRV_NM_BLK_DEV = "SRV_NM_BLK_DEV"; // Service Personal
															// Details
	public static String SRV_NM_OFR_INTR = "SRV_NM_OFR_INTR"; // Service
																// Personal
																// Details
	public static String SRV_NM_COUNTRY_CD = "SRV_NM_COUNTRY_CD"; // Service
																	// Country
																	// code
	public static String SRV_NM_PTRL_NRM_DETS = "SRV_NM_PTRL_NRM_DETS"; // service
																		// petrol
																		// details
	public static String SRV_NM_PTRL_NRM_SBT = "SRV_NM_PTRL_NRM_SBT"; // Service
																		// Petrol
																		// Submit
	public static String SRV_NM_UNLK_ID = "SRV_NM_UNLK_ID"; // Service Unlock ID
	public static String SRV_NM_LV_M_APR = "SRV_NM_LV_M_APR"; // Service
																// multiple
																// leave approve
																// reject
	public static String SRV_NM_MS_M_APR = "SRV_NM_MS_M_APR"; // Service
																// multiple
																// muster
																// approve
																// reject
	public static String SRV_NM_LC_P_CON = "SRV_NM_LC_P_CON"; // Service
																// Personal
																// Details
	public static String SRV_NM_MS_P_CON = "SRV_NM_MS_P_CON"; // Service
																// Personal
																// Details
	public static String SRV_NM_LATE_PNCH_DETS = "SRV_NM_LATE_PNCH_DETS"; // Late
																			// punch
																			// details
	public static String SRV_NM_LATE_PNCH_APR = "SRV_NM_LATE_PNCH_APR"; // Late
																		// punch
																		// details
																		// approve
																		// reject
	public static String SRV_NM_LATE_PNCH_HIST = "SRV_NM_LATE_PNCH_HIST"; // Late
																			// punch
																			// details
																			// approve
																			// reject
	public static String SRV_NM_LATE_PNCH_DT = "SRV_NM_LATE_PNCH_DT"; // Late
																		// punch
																		// details
																		// approve
																		// reject
	public static String SRV_NM_LATE_PNCH_SUB = "SRV_NM_LATE_PNCH_SUB"; // Late
																		// punch
																		// details
																		// approve
																		// reject
	public static String SRV_NM_LP_PND_CONT = "SRV_NM_LP_PND_CONT"; // Service
																	// pending
																	// leave
																	// count
	public static String SRV_NM_MY_TM = "SRV_NM_MY_TM"; // Service My team
	public static String SRV_NM_EMG_HELP = "SRV_NM_EMG_HELP"; // Service
																// Emergency
																// Helpline
	public static String SRV_NM_RESET_MPIN = "SRV_NM_RESET_MPIN"; // Service
																	// Reset
																	// MPIN
	public static String SRV_NM_DE_REG = "SRV_NM_DE_REG"; // Service Reset MPIN
	public static String SRV_NM_MPIN = "SRV_NM_MPIN"; // Service Menu MPIN
														// verify
	public static String SRV_NM_REG_LST = "SRV_NM_REG_LST"; // Service Reset
															// MPIN
	public static String SRV_NM_ESOP_DET = "SRV_NM_ESOP_DET"; // Service ESOP
																// details
	public static String SRV_NM_PMS_TKN = "SRV_NM_PMS_TKN"; // Service PMS token
	public static String SRV_NM_EMP_DETLS = "SRV_NM_EMP_DETLS";
	//
	/**
	 * Service Investment
	 */
	public static String SRV_NM_INVST_80CC = "SRV_NM_INVST_80CC"; // Service
																	// 80CCFileUpload
																	// Info

	public static String SRV_NM_INVST_LTA = "SRV_NM_INVST_LTA"; // Service LTA
																// FileUpload
																// Info
	public static String SRV_NM_INVST_OTH_INV = "SRV_NM_INVST_OTH_INV"; // Service
																		// OTH
																		// Inv
																		// File
																		// upload
	public static String SRV_NM_INVST_ALL_LTA_DTL = "SRV_NM_INVST_ALL_LTA_DTL"; // Service
																				// All
																				// LTA
																				// Details
	public static String SRV_NM_INVST_RNT = "SRV_NM_INVST_RNT"; // Service Rent
																// File Upload
	public static String SRV_NM_INVST_ADDTL_INC = "SRV_NM_INVST_ADDTL_INC"; // Service
																			// Additional
																			// Inc
																			// Upload
																			// Info
	public static String SRV_NM_INVST_MDCL = "SRV_NM_INVST_MDCL"; // Service
																	// Medical
																	// Upload
																	// Info
	public static String SRV_NM_INVST_TLPNE = "SRV_NM_INVST_TLPNE"; // Service
																	// Telephone
																	// Upload
	public static String SRV_NM_INVST_LOS_HSG_PRP = "SRV_NM_INVST_LOS_HSG_PRP"; // Service
																				// LossOnHousingPropInfo
	public static String SRV_NM_INVST_PTRL = "SRV_NM_INVST_PTRL"; // Service
																	// Petrol
																	// Upload
																	// Info
	public static String SRV_NM_INVST_UPLD_TY = "SRV_NM_INVST_UPLD_TY"; // Service
																		// Upload
																		// by
																		// type
	public static String SRV_NM_INVST_UPDT_FL_HR4U = "SRV_NM_INVST_UPDT_FL_HR4U"; // Service
																					// Upload
																					// by
																					// type
	public static String SRV_NM_INVST_UPDT_FL_HR4U_AMNT = "SRV_NM_INVST_UPDT_FL_HR4U_AMNT"; // Service
																							// Upload
																							// by
																							// type
	public static String SRV_NM_INVST_UPDT_PROPTY = "SRV_NM_INVST_UPDT_PROPTY"; // Service
																				// Update
																				// Property

	public static String SRV_NM_INVST_DELETE = "SRV_NM_INVST_DELETE";
	public static String SRV_NM_INVST_VIEW = "SRV_NM_INVST_VIEW";
	public static String SRV_NM_INVST_UPDT_LTA_INSRT = "SRV_NM_INVST_UPDT_LTA_INSRT"; // Service
																						// Update
																						// Property
	public static String SRV_NM_INVST_SMRY = "SRV_NM_INVST_SMRY"; // Service
																	// Investment
																	// Summary
	public static String SRV_NM_INVST_UPDT_SUMMARY = "SRV_NM_INVST_UPDT_SUMMARY"; // Service
																					// Update
																					// Property
	public static String SRV_NM_INVST_UPDT_LENDER = "SRV_NM_INVST_UPDT_LENDER"; // Lender
																				// Service
																				// Update
																				// Property
	public static String SRV_NM_TALK_2_HR = "SRV_NM_TALK_2_HR"; // Service TALK
																// 2 HR
	public static String SRV_NM_DNA_AWARD = "SRV_NM_DNA_AWARD"; // Service TALK
																// 2 HR
	public static String SRV_NM_INVST_LENDER_DELETE = "SRV_NM_INVST_LENDER_DELETE";

	/**
	 * Service req Parameters
	 */
	public static String SRV_NM = "SRV_NM"; // Service Name
	public static String SRV_UID = "SRV_UID"; // Service User Id
	public static String SRV_PASS = "SRV_PASS"; // Service Password
	public static String SRV_ADT = "SRV_ADT"; // request Audit section
	public static String SRV_NTFCN = "SRV_NTFCN"; // request notification
													// section
	public static String SRV_DATA = "SRV_DATA"; // request data section
	/**
	 * Service Resp Parameters
	 */
	public static String SRV_RSP_EXCPT = "SRV_RSP_EXCPT"; // Service Exception
															// Msg
	public static String SRV_RSP_STS = "SRV_RSP_STS"; // Service Response Status
														// 200-OK 500-Error
	public static String SRV_RSP_DATA = "SRV_RSP_DATA";
	public static String _200 = "200"; // Response data appENed in this feild
	/**
	 * Response Parameters
	 */
	// public static String RESP_EXCPTN="RESP_EXCPTN";
	// public static String RESP_STS="RESP_STS";
	// public static String RESP_DATA="RESP_DATA";
	/**
	 * Response Parameters
	 */
	/**
	 * Audit Parameters
	 */
	public static String ADT_TYP_CD = "ADT_TYP_CD"; // Audit TYP code
	public static String ADT_UID = "ADT_UID"; // Audit user id
	public static String ADT_BWSR = "ADT_BWSR"; // browser name
	public static String ADT_IP = "ADT_IP"; // ip address
	public static String ADT_DT_TME = "ADT_DT_TME"; // time
	public static String ADT_DATA = "ADT_DATA"; // Any data to be audited
	public static String ADT_RSTR = "ADT_RSTR"; // Requested by E-Engage /
												// P-Peoplesoft
	public static String ADT_GRADE = "ADT_GRADE"; // Requested by E-Engage /
													// P-Peoplesoft
	/**
	 * Audit Parameters
	 */
	/**
	 * Notification Parameters
	 */
	public static String NTFCN_FRM_ID = "NTFCN_FRM_ID"; // from user id
	public static String NTFCN_TO_ID = "NTFCN_TO_ID"; // to user id
	public static String NTFCN_TO_NM = "NTFCN_TO_NM"; // to user name
	public static String NTFCN_FRM_NM = "NTFCN_FRM_NM"; // from user name
	public static String NTFCN_PARAMS = "NTFCN_PARAMS"; // dynamic parameters to
														// invoke URL
	public static String NTFCN_TYP_CD = "NTFCN_TYP_CD"; // TYP code to identify
														// later
	/**
	 * Notification Parameters
	 */
	/**
	 * Login Parameters
	 */
	public static String LGN_REQ_EMP_ID = "LGN_REQ_EMP_ID"; // user id to fetch
															// data for
	public static String LGN_RSP_EMP_ID = "LGN_RSP_EMP_ID"; // response employee
															// id
	public static String LGN_RSP_GRADE = "LGN_RSP_GRADE"; // user grade
	public static String LGN_RSP_EMP_NM = "LGN_RSP_EMP_NM"; // user full name
	public static String LGN_RSP_EMP_TYP = "LGN_RSP_EMP_TYP"; // Employee type
																// M- manager or
																// E- Employee
	public static String LGN_RSP_EMP_LOC = "LGN_RSP_EMP_LOC"; // Employee type
																// M- manager or
																// E- Employee
	public static String LGN_RSP_EMP_NODE = "LGN_RSP_EMP_NODE"; // Employee type
																// M- manager or
																// E- Employee
	/**
	 * Login Parameters
	 */
	/**
	 * Leave Apply Parameters
	 */
	public static String LV_AP_REQ_TRXN_ID = "LV_AP_REQ_TRXN_ID"; // transaction
																	// id
																	// Default-0
	public static String LV_AP_REQ_BG_DT = "LV_AP_REQ_BG_DT"; // Begin Date
	public static String LV_AP_REQ_EN_DT = "LV_AP_REQ_EN_DT"; // END Date
	public static String LV_AP_REQ_TYP_CD = "LV_AP_REQ_TYP_CD"; // Leave Type
																// Code
	public static String LV_AP_REQ_RSN_CD = "LV_AP_REQ_RSN_CD"; // Leave Reason
																// Code
	public static String LV_AP_REQ_CMTS = "LV_AP_REQ_CMTS"; // Employee comments
	public static String LV_AP_RSP_NTFCN = "LV_AP_RSP_NTFCN"; // Successfull
																// notification
	/**
	 * Leave Apply Parameters
	 */
	/**
	 * Leave History List Parameters
	 */
	public static String LV_H_REQ_FRM_DT = "LV_H_REQ_FRM_DT"; // History from
																// date
	public static String LV_H_REQ_TO_DT = "LV_H_REQ_TO_DT"; // History to date
	public static String LV_H_RSP_TRXN_ID = "LV_H_RSP_TRXN_ID"; // Transaction
																// id
	public static String LV_H_RSP_BG_DT = "LV_H_RSP_BG_DT"; // begin date
	public static String LV_H_RSP_EN_DT = "LV_H_RSP_EN_DT"; // end date
	public static String LV_H_RSP_TYP_CD = "LV_H_RSP_TYP_CD"; // Type code
	public static String LV_H_RSP_TYP_D = "LV_H_RSP_TYP_D"; // type Description
	public static String LV_H_RSP_RSN_CD = "LV_H_RSP_RSN_CD"; // Reason code
	public static String LV_H_RSP_RSN_D = "LV_H_RSP_RSN_D"; // Reason
															// Description
	public static String LV_H_RSP_STS = "LV_H_RSP_STS"; // Leave Status
	public static String LV_H_RSP_SPRVR_ID = "LV_H_RSP_SPRVR_ID"; // Supervisor
																	// id
	public static String LV_H_RSP_SPRVR_NM = "LV_H_RSP_SPRVR_NM"; // Supervisor
																	// name
	public static String LV_H_RSP_DRTN = "LV_H_RSP_DRTN"; // Duration
	public static String LV_H_RSP_ACT_DT = "LV_H_RSP_ACT_DT"; // Action date
	/**
	 * Leave History List Parameters
	 */
	/**
	 * Leave History Details Parameters
	 */
	public static String LV_H_D_RSP_TRXN_ID = "LV_H_D_RSP_TRXN_ID"; // transaction
																	// id
	public static String LV_H_D_RSP_BG_DT = "LV_H_D_RSP_BG_DT"; // begin date
	public static String LV_H_D_RSP_EN_DT = "LV_H_D_RSP_EN_DT"; // end date
	public static String LV_H_D_RSP_TYP_CD = "LV_H_D_RSP_TYP_CD"; // type code
	public static String LV_H_D_RSP_TYP_D = "LV_H_D_RSP_TYP_D"; // type
																// description
	public static String LV_H_D_RSP_RSN_CD = "LV_H_D_RSP_RSN_CD"; // reason code
	public static String LV_H_D_RSP_RSN_D = "LV_H_D_RSP_RSN_D"; // reason
																// description
	public static String LV_H_D_RSP_STS = "LV_H_D_RSP_STS"; // status
	public static String LV_H_D_RSP_SPRVR_ID = "LV_H_D_RSP_SPRVR_ID"; // Supervisor
																		// id
	public static String LV_H_D_RSP_SPRVR_NM = "LV_H_D_RSP_SPRVR_NM"; // supervisor
																		// name
	public static String LV_H_D_RSP_DRTN = "LV_H_D_RSP_DRTN"; // duration
	public static String LV_H_D_RSP_ACT_DT = "LV_H_D_RSP_ACT_DT"; // Action Date
	public static String LV_H_D_RSP_CMTS = "LV_H_D_RSP_CMTS"; // Employee
																// comments
	public static String LV_H_D_RSP_APPR_CMTS = "LV_H_D_RSP_APPR_CMTS"; // Approver
																		// comments
	/**
	 * Leave History Details Parameters
	 */
	/**
	 * Leave Balance Parameters
	 */
	public static String LV_BL_REQ_EMP_ID = "LV_BL_REQ_EMP_ID"; // Leave Balance
																// of employees
	public static String LV_BL_RSP_TYP_D = "LV_BL_RSP_TYP_D"; // Leave Type
																// description
	public static String LV_BL_RSP_COUNT = "LV_BL_RSP_COUNT"; // Count of leaves
																// pending
	/**
	 * Leave Approval list Parameters
	 */
	public static String LV_P_AP_REQ_STS = "LV_P_AP_REQ_STS"; // Status, P-
																// Pending
	public static String LV_P_AP_RSP_EMP_NM = "LV_P_AP_RSP_EMP_NM"; // Employee
																	// name
	public static String LV_P_AP_RSP_EMP_ID = "LV_P_AP_RSP_EMP_ID"; // Employee
																	// ID
	public static String LV_P_AP_RSP_BG_DT = "LV_P_AP_RSP_BG_DT"; // begin date
	public static String LV_P_AP_RSP_EN_DT = "LV_P_AP_RSP_EN_DT"; // end date
	public static String LV_P_AP_RSP_TYP_CD = "LV_P_AP_RSP_TYP_CD"; // leave
																	// type code
	public static String LV_P_AP_RSP_TYP_D = "LV_P_AP_RSP_TYP_D"; // leave type
																	// description
	public static String LV_P_AP_RSP_RSN_D = "LV_P_AP_RSP_RSN_D"; // reason
																	// description
	public static String LV_P_AP_RSP_RSN_CD = "LV_P_AP_RSP_RSN_CD"; // reason
																	// code
	public static String LV_P_AP_RSP_STS = "LV_P_AP_RSP_STS"; // status
	public static String LV_P_AP_RSP_TRXN_ID = "LV_P_AP_RSP_TRXN_ID"; // transaction
																		// id
	public static String LV_P_AP_RSP_CMTS = "LV_P_AP_RSP_CMTS"; // Employee
																// comments
	public static String LV_P_AP_RSP_CNT = "LV_P_AP_RSP_CNT"; // Pending count
	/**
	 * Leave Approval Details Parameters
	 */
	public static String LV_P_AP_D_REQ_TRXN_ID = "LV_P_AP_D_REQ_TRXN_ID"; // Leave
																			// transaction
																			// id
	public static String LV_P_AP_D_RSP_TRXN_ID = "LV_P_AP_D_RSP_TRXN_ID"; // transaction
																			// id
	public static String LV_P_AP_D_RSP_EMP_ID = "LV_P_AP_D_RSP_EMP_ID"; // employee
																		// id
	public static String LV_P_AP_D_RSP_EMP_NM = "LV_P_AP_D_RSP_EMP_NM"; // empployee
																		// name
	public static String LV_P_AP_D_RSP_BG_DT = "LV_P_AP_D_RSP_BG_DT"; // begin
																		// date
	public static String LV_P_AP_D_RSP_EN_DT = "LV_P_AP_D_RSP_EN_DT"; // end
																		// date
	public static String LV_P_AP_D_RSP_TYP_CD = "LV_P_AP_D_RSP_TYP_CD"; // type
																		// code
	public static String LV_P_AP_D_RSP_TYP_D = "LV_P_AP_D_RSP_TYP_D"; // type
																		// description
	public static String LV_P_AP_D_RSP_RSN_CD = "LV_P_AP_D_RSP_RSN_CD"; // reason
																		// code
	public static String LV_P_AP_D_RSP_RSN_D = "LV_P_AP_D_RSP_RSN_D"; // reason
																		// description
	public static String LV_P_AP_D_RSP_AVLD = "LV_P_AP_D_RSP_AVLD"; // leaves
																	// availed
	public static String LV_P_AP_D_RSP_CMTS = "LV_P_AP_D_RSP_CMTS"; // employee
																	// comments
	/**
	 * Leave Approve Reject Parameters
	 */
	public static String LV_P_AP_AR_REQ_TRXN_ID = "LV_P_AP_AR_REQ_TRXN_ID"; // transaction
																			// id
	public static String LV_P_AP_AR_REQ_EMP_ID = "LV_P_AP_AR_REQ_EMP_ID"; // Employee
																			// id
	public static String LV_P_AP_AR_REQ_BG_DT = "LV_P_AP_AR_REQ_BG_DT"; // begin
																		// date
	public static String LV_P_AP_AR_REQ_EN_DT = "LV_P_AP_AR_REQ_EN_DT"; // end
																		// date
	public static String LV_P_AP_AR_REQ_TYP_CD = "LV_P_AP_AR_REQ_TYP_CD"; // type
																			// code
	public static String LV_P_AP_AR_REQ_RSN_CD = "LV_P_AP_AR_REQ_RSN_CD"; // reason
																			// code
	public static String LV_P_AP_AR_REQ_APPR_CMTS = "LV_P_AP_AR_REQ_APPR_CMTS"; // approver
																				// comments
	public static String LV_P_AP_AR_REQ_STS_CD = "LV_P_AP_AR_REQ_STS_CD"; // Approve
																			// Rejected
																			// A
																			// /
																			// D
	/**
	 * Team CalENdar Parameters
	 */
	public static String TM_CL_REQ_BG_DT = "TM_CL_REQ_BG_DT"; // begin date
	public static String TM_CL_REQ_EN_DT = "TM_CL_REQ_EN_DT"; // end date
	public static String TM_CL_RSP_EMP_ID = "TM_CL_RSP_EMP_ID"; // employee id
	public static String TM_CL_RSP_EMP_NM = "TM_CL_RSP_EMP_NM"; // employee name
	public static String TM_CL_RSP_BG_DT = "TM_CL_RSP_BG_DT"; // begin date
	public static String TM_CL_RSP_EN_DT = "TM_CL_RSP_EN_DT"; // end date
	public static String TM_CL_RSP_TRXN_ID = "TM_CL_RSP_TRXN_ID"; // transaction
																	// id
	public static String TM_CL_RSP_STS = "TM_CL_RSP_STS"; // status P / A / D
	/**
	 * Muster Update Parameters
	 */
	public static String MS_UP_LST_REQ_EMP_ID = "MS_UP_LST_REQ_EMP_ID"; // Employee
																		// id
	public static String MS_UP_LST_REQ_BG_DT = "MS_UP_LST_REQ_BG_DT"; // Begin
																		// date
	public static String MS_UP_LST_REQ_EN_DT = "MS_UP_LST_REQ_EN_DT"; // End
																		// Date
	public static String MS_UP_LST_RSP_DT = "MS_UP_LST_RSP_DT"; // Dates
	/**
	 * Muster Update Submit Parameters
	 */
	public static String MS_UP_S_REQ_DT = "MS_UP_S_REQ_DT"; // Date
	public static String MS_UP_S_REQ_EMP_ID = "MS_UP_S_REQ_EMP_ID"; // employee
																	// id
	public static String MS_UP_S_REQ_IN_TM = "MS_UP_S_REQ_IN_TM"; // in time
	public static String MS_UP_S_REQ_OT_TM = "MS_UP_S_REQ_OT_TM"; // out time
	public static String MS_UP_S_REQ_TRC_CD = "MS_UP_S_REQ_TRC_CD"; // TRC code
	public static String MS_UP_S_RSP_NTFCN = "MS_UP_S_RSP_NTFCN"; // notifications
	/**
	 * Muster History List Parameters
	 */
	public static String MS_H_REQ_EMP_ID = "MS_H_REQ_EMP_ID"; // employee id
	public static String MS_H_RSP_DT = "MS_H_RSP_DT"; // date
	public static String MS_H_RSP_IN_TM = "MS_H_RSP_IN_TM"; // in time
	public static String MS_H_RSP_OT_TM = "MS_H_RSP_OT_TM"; // out time
	public static String MS_H_RSP_TRC_CD = "MS_H_RSP_TRC_CD"; // trc code
	public static String MS_H_RSP_TRC_D = "MS_H_RSP_TRC_D"; // trc description
	public static String MS_H_RSP_QTY = "MS_H_RSP_QTY"; // quantity
	public static String MS_H_RSP_STS = "MS_H_RSP_STS"; // status NA / A / D
	/**
	 * Muster PENding List Parameters
	 */
	public static String MS_P_AP_REQ_EMP_ID = "MS_P_AP_REQ_EMP_ID"; // employee
																	// id
	public static String MS_P_AP_RSP_EMP_ID = "MS_P_AP_RSP_EMP_ID"; // employee
																	// id
	public static String MS_P_AP_RSP_EMP_NM = "MS_P_AP_RSP_EMP_NM"; // employee
																	// name
	public static String MS_P_AP_RSP_DT = "MS_P_AP_RSP_DT"; // date
	public static String MS_P_AP_RSP_IN_TM = "MS_P_AP_RSP_IN_TM"; // in time
	public static String MS_P_AP_RSP_OT_TM = "MS_P_AP_RSP_OT_TM"; // out time
	public static String MS_P_AP_RSP_SQN_NO = "MS_P_AP_RSP_SQN_NO"; // sequence
																	// no
	public static String MS_P_AP_RSP_TRC_CD = "MS_P_AP_RSP_TRC_CD"; // trc code
	public static String MS_P_AP_RSP_TRC_D = "MS_P_AP_RSP_TRC_D"; // trc
																	// description
	/**
	 * Muster PENing details Parameters
	 */
	public static String MS_P_AP_D_REQ_DT = "MS_P_AP_D_REQ_DT"; // date
	public static String MS_P_AP_D_REQ_EMP_ID = "MS_P_AP_D_REQ_EMP_ID"; // employee
																		// id
	public static String MS_P_AP_D_REQ_SQN_NO = "MS_P_AP_D_REQ_SQN_NO"; // sequence
																		// no
	public static String MS_P_AP_D_REQ_TRC_CD = "MS_P_AP_D_REQ_TRC_CD"; // trc
																		// code
	public static String MS_P_AP_D_RSP_EMP_ID = "MS_P_AP_D_RSP_EMP_ID"; // employee
																		// id
	public static String MS_P_AP_D_RSP_EMP_NM = "MS_P_AP_D_RSP_EMP_NM"; // employee
																		// name
	public static String MS_P_AP_D_RSP_DT = "MS_P_AP_D_RSP_DT"; // date
	public static String MS_P_AP_D_RSP_IN_TM = "MS_P_AP_D_RSP_IN_TM"; // in time
	public static String MS_P_AP_D_RSP_OT_TM = "MS_P_AP_D_RSP_OT_TM"; // out
																		// time
	public static String MS_P_AP_D_RSP_TRC_CD = "MS_P_AP_D_RSP_TRC_CD"; // trc
																		// code
	public static String MS_P_AP_D_RSP_TRC_D = "MS_P_AP_D_RSP_TRC_D"; // trc
																		// description
	public static String MS_P_AP_D_RSP_SQN_NO = "MS_P_AP_D_RSP_SQN_NO"; // sequence
																		// no
	/**
	 * Muster PENing details Parameters
	 */
	/**
	 * Muster PENing Approve Parameters
	 */
	public static String MS_P_AP_AR_REQ_DT = "MS_P_AP_AR_REQ_DT"; // date
	public static String MS_P_AP_AR_REQ_EMP_ID = "MS_P_AP_AR_REQ_EMP_ID"; // employee
																			// id
	public static String MS_P_AP_AR_REQ_SQN_ID = "MS_P_AP_AR_REQ_SQN_ID"; // sequence
																			// id
	public static String MS_P_AP_AR_REQ_TRC_CD = "MS_P_AP_AR_REQ_TRC_CD"; // trc
																			// code
	public static String MS_P_AP_AR_REQ_STS = "MS_P_AP_AR_REQ_STS"; // status Y
																	// / N
	public static String MS_P_AP_AR_REQ_IN_TM = "MS_P_AP_AR_REQ_IN_TM"; // in
																		// time
	public static String MS_P_AP_AR_REQ_OT_TM = "MS_P_AP_AR_REQ_OT_TM"; // out
																		// time
	/**
	 * Muster PENing Approve Parameters
	 */
	/**
	 * Salary details Parameters
	 */
	public static String MC_SAL_REQ_EMP_ID = "MC_SAL_REQ_EMP_ID"; // employee id
																	// //
																	// encrypted
	public static String MC_SAL_REQ_GEN_PASS = "MC_SAL_REQ_GEN_PASS"; // Generic
																		// password
	public static String MC_SAL_RSP_SAL_D = "MC_SAL_RSP_SAL_D"; // Salary
																// Details
	/**
	 * Salary details Parameters
	 */
	/**
	 * Expense History Parameters response
	 */
	public static String EXP_HIST_AMT = "EXP_HIST_AMT"; // Amount
	public static String EXP_HIST_CLM_OF_DT = "EXP_HIST_CLM_OF_DT"; //
	public static String EXP_HIST_CLM_DT = "EXP_HIST_CLM_DT"; //
	public static String EXP_HIST_FORM = "EXP_HIST_FORM"; // Form
	public static String EXP_HIST_FORM_STAT = "EXP_HIST_FORM_STAT"; // Status
	public static String EXP_HIST_PROC_DT = "EXP_HIST_PROC_DT"; //
	/**
	 * Expense History Parameters response
	 */
	/**
	 * Expense History details Parameters response
	 */
	public static String EXP_DET_REQ_FORM = "EXP_DET_REQ_FORM"; // Form request
	public static String EXP_DET_RES_CMN = "EXP_DET_RES_CMN"; // Common field
	public static String EXP_DET_RES_DTS = "EXP_DET_RES_DTS"; // Details field-
																// return list
	public static String EXP_DET_RES_FORM = "EXP_DET_RES_FORM"; // Form responce
	public static String EXP_DET_RES_STS = "EXP_DET_RES_STS"; // Status
	public static String EXP_DET_RES_TOT = "EXP_DET_RES_TOT"; // Total
																// Conveyance
	public static String EXP_DET_RES_APP = "EXP_DET_RES_APP"; // Approver Name
	public static String EXP_DET_RES_AMT = "EXP_DET_RES_AMT"; // Amount
	public static String EXP_DET_RES_VIST = "EXP_DET_RES_VISI"; // Number of
																// Visits
	public static String EXP_DET_RES_DIST = "EXP_DET_RES_DIST"; // Approx Dist.
	public static String EXP_DET_RES_DT = "EXP_DET_RES_DT"; // Date
	public static String EXP_DET_RES_FRM = "EXP_DET_RES_FRM"; // From place
	public static String EXP_DET_RES_NAR = "EXP_DET_RES_NAR"; // Narration
	public static String EXP_DET_RES_TO = "EXP_DET_RES_TO"; // To place
	public static String EXP_DET_RES_TRA = "EXP_DET_RES_TRA"; // Transport Mode
	public static String EXP_DET_RES_DEL_STS = "EXP_DET_RES_DEL_STS"; // Delete
																		// status
	/**
	 * Expense History details Parameters response
	 */
	/**
	 * Expense History details Delete Parameters response
	 */
	public static String EXP_DEL_REQ_FORM = "EXP_DEL_REQ_FORM"; // Delete form
																// request
	public static String EXP_DEL_REQ_EMPID = "EXP_DEL_REQ_EMPID";
	public static String EXP_DEL_REQ_CD = "EXP_DEL_REQ_CD";
	/**
	 * Expense History details Delete Parameters response
	 */
	/**
	 * Expense Apply form Parameters request
	 */
	public static String EXP_PSBMT_REQ_FORM = "exp_PSBMT_REQ_FORM";
	public static String EXP_PSBMT_RES_SESS = "EXP_PSBMT_REQ_SESS";
	public static String EXP_PSBMT_RES_APPLIST = "EXP_PSBMT_RES_APPLIST";
	public static String EXP_PSBMT_RES_APPID = "EXP_PSBMT_RES_APPID";
	public static String EXP_PSBMT_RES_APPNM = "EXP_PSBMT_RES_APPNM";
	public static String EXP_PSBMT_RES_TRSLIST = "EXP_PSBMT_RES_TRSLIST";
	public static String EXP_PSBMT_RES_TRSID = "EXP_PSBMT_RES_TRSID";
	public static String EXP_PSBMT_RES_TRSMOD = "EXP_PSBMT_RES_TRSMOD";
	/**
	 * Expense Apply Parameters request
	 */
	/**
	 * Petrol Apply form Parameters request
	 */
	/*
	 * public static String EXP_PSBMT_REQ_FORM = "EXP_PSBMT_REQ_FORM"; public
	 * static String EXP_PSBMT_RES_SESS = "EXP_PSBMT_REQ_SESS"; public static
	 * String EXP_PSBMT_RES_APPLIST = "EXP_PSBMT_RES_APPLIST"; public static
	 * String EXP_PSBMT_RES_APPID = "EXP_PSBMT_RES_APPID"; public static String
	 * EXP_PSBMT_RES_APPNM = "EXP_PSBMT_RES_APPNM"; public static String
	 * EXP_PSBMT_RES_TRSLIST = "EXP_PSBMT_RES_TRSLIST"; public static String
	 * EXP_PSBMT_RES_TRSID = "EXP_PSBMT_RES_TRSID"; public static String
	 * EXP_PSBMT_RES_TRSMOD = "EXP_PSBMT_RES_TRSMOD"; /** Petrol Apply
	 * Parameters request
	 */
	/**
	 * Expense Apply form submit Parameters request
	 */
	public static String EXP_SBMT_REQ_APP = "EXP_SBMT_REQ_APP";
	public static String EXP_SBMT_REQ_FORM = "EXP_SBMT_REQ_FORM";
	public static String EXP_SBMT_REQ_DT = "EXP_SBMT_REQ_DT";
	public static String EXP_SBMT_REQ_FRM = "EXP_SBMT_REQ_FRM";
	public static String EXP_SBMT_REQ_TO = "EXP_SBMT_REQ_TO";
	public static String EXP_SBMT_REQ_DST = "EXP_SBMT_REQ_DST";
	public static String EXP_SBMT_REQ_TR = "EXP_SBMT_REQ_TR";
	public static String EXP_SBMT_REQ_AMT = "EXP_SBMT_REQ_AMT";
	public static String EXP_SBMT_REQ_VIST = "EXP_SBMT_REQ_VIST";
	public static String EXP_SBMT_REQ_CMTS = "EXP_SBMT_REQ_CMTS";
	public static String EXP_SBMT_REQ_SESS = "EXP_SBMT_REQ_SESS";
	/**
	 * Expense Apply submit Parameters request
	 */
	/**
	 * Expense Pending Approvals list Parameters response
	 */
	public static String EXP_PND_RES_AMT = "EXP_PND_RES_AMT";
	public static String EXP_PND_RES_CLM_OF_DT = "EXP_PND_RES_CLM_OF_DT";
	public static String EXP_PND_RES_CLM_DT = "EXP_PND_RES_CLM_DT";
	public static String EXP_PND_RES_FORM = "EXP_PND_RES_FORM";
	public static String EXP_PND_RES_FORM_STAT = "EXP_PND_RES_FORM_STAT";
	public static String EXP_PND_RES_PROC_DT = "EXP_PND_RES_PROC_DT";
	public static String EXP_PND_RES_PROC_GR = "EXP_PND_RES_PROC_GR"; // Grade
	public static String EXP_PND_RES_PROC_NM = "EXP_PND_RES_PROC_NM"; // emp
																		// Name
	public static String EXP_PND_RES_PROC_RE = "EXP_PND_RES_PROC_RE"; // Reason/Claim
																		// for
	/**
	 * Expense Pending Approvals list Parameters response
	 */
	/**
	 * Expense Pending Approvals Details Parameters response
	 */
	public static String EXP_PND_APP_REQ_FORM = "EXP_PND_DET_REQ_FORM";
	public static String EXP_PND_APP_RES_FORM = "EXP_PND_APP_RES_FORM";
	public static String EXP_PND_APP_RES_NM = "EXP_PND_APP_RES_NM";
	public static String EXP_PND_APP_RES_TOT = "EXP_PND_APP_RES_TOT";
	public static String EXP_PND_APP_RES_DATA = "EXP_PND_APP_RES_DATA";
	public static String EXP_PND_APP_RES_DT = "EXP_PND_APP_RES_DT";
	public static String EXP_PND_APP_RES_AMT = "EXP_PND_APP_RES_AMT";
	public static String EXP_PND_APP_RES_VIST = "EXP_PND_APP_RES_VIST";
	public static String EXP_PND_APP_RES_DST = "EXP_PND_APP_RES_DST";
	public static String EXP_PND_APP_RES_FRM = "EXP_PND_APP_RES_FRM";
	public static String EXP_PND_APP_RES_TO = "EXP_PND_APP_RES_TO";
	public static String EXP_PND_APP_RES_NAR = "EXP_PND_APP_RES_NAR";
	public static String EXP_PND_APP_RES_TR = "EXP_PND_APP_RES_TR";
	/**
	 * Expense Pending Approvals Details Parameters response
	 */
	/**
	 * Expense Pending Approvals Parameters response - Approve Reject
	 */
	public static String EXP_APP_REJ_REQ_FORM = "EXP_APP_REJ_REQ_FORM";
	public static String EXP_APP_REJ_REQ_CD = "EXP_APP_REJ_REQ_CD";
	public static String EXP_APP_REJ_REQ_EMP = "EXP_APP_REJ_REQ_EMP";
	public static String EXP_APP_REJ_REQ_CMTS = "EXP_APP_REJ_REQ_CMTS";
	/**
	 * Expense Pending Approvals Details Parameters response
	 */
	/**
	 * Offer Type - request
	 */
	public static String OFR_TYP_REQ_ID = "OFR_TYP_REQ_ID";
	/**
	 * Home offer - location response
	 */
	public static String OFR_HOME_LOC_RES_ID = "OFR_HOME_LOC_RES_ID";
	public static String OFR_HOME_LOC_RES_CT = "OFR_HOME_LOC_RES_CT";
	public static String OFR_HOME_LOC_RES_ST = "OFR_HOME_LOC_RES_ST";
	public static String OFR_HOME_LOC_RES_LAT = "OFR_HOME_LOC_RES_LAT";
	public static String OFR_HOME_LOC_RES_LNG = "OFR_HOME_LOC_RES_LNG";
	public static String OFR_HOME_LOC_RES_REC = "OFR_HOME_LOC_RES_REC";
	public static String OFR_HOME_LOC_RES_TOT = "OFR_HOME_LOC_RES_TOT";
	/**
	 * Home offer - list request
	 */
	public static String OFR_HOME_LOC_REQ_ID = "OFR_HOME_LOC_REQ_ID";
	/**
	 * Home offer - list response
	 */
	public static String OFR_HOME_LST_RES_ID = "OFR_HOME_LST_RES_ID";
	public static String OFR_HOME_LST_RES_TIT = "OFR_HOME_LST_RES_TIT";
	public static String OFR_HOME_LST_RES_SBT = "OFR_HOME_LST_RES_SBT";
	public static String OFR_HOME_LST_RES_OFR = "OFR_HOME_LST_RES_OFR";
	public static String OFR_HOME_LST_RES_IMG = "OFR_HOME_LST_RES_IMG";
	public static String OFR_HOME_LST_RES_LOC = "OFR_HOME_LST_RES_LOC";
	public static String OFR_HOME_LST_RES_VL = "OFR_HOME_LST_RES_VL";
	public static String OFR_HOME_LST_RES_REC = "OFR_HOME_LST_RES_REC";
	/**
	 * Home offer - details request
	 */
	public static String OFR_HOME_LST_REQ_ID = "OFR_HOME_LST_REQ_ID";
	/**
	 * Home offer - details response
	 */
	public static String OFR_HOME_DETS_RES_ID = "OFR_HOME_DETS_RES_ID";
	public static String OFR_HOME_DETS_RES_TIT = "OFR_HOME_DETS_RES_TIT";
	public static String OFR_HOME_DETS_RES_SBT = "OFR_HOME_DETS_RES_SBT";
	public static String OFR_HOME_DETS_RES_DET = "OFR_HOME_DETS_RES_DET";
	public static String OFR_HOME_DETS_RES_OFR = "OFR_HOME_DETS_RES_OFR";
	public static String OFR_HOME_DETS_RES_IMG = "OFR_HOME_DETS_RES_IMG";
	public static String OFR_HOME_DETS_RES_CONT1 = "OFR_HOME_DETS_RES_CONT1";
	public static String OFR_HOME_DETS_RES_CONT2 = "OFR_HOME_DETS_RES_CONT2";
	public static String OFR_HOME_DETS_RES_EML = "OFR_HOME_DETS_RES_EML";
	public static String OFR_HOME_DETS_RES_URL = "OFR_HOME_DETS_RES_URL";
	public static String OFR_HOME_DETS_RES_NM = "OFR_HOME_DETS_RES_NM";
	public static String OFR_HOME_DETS_RES_LOC = "OFR_HOME_DETS_RES_LOC";
	public static String OFR_HOME_DETS_RES_SDT = "OFR_HOME_DETS_RES_SDT";
	public static String OFR_HOME_DETS_RES_VL = "OFR_HOME_DETS_RES_VL";
	public static String OFR_HOME_DETS_RES_REC = "OFR_HOME_DETS_RES_REC";
	/**
	 * Car offer - Brand response
	 */
	public static String OFR_CAR_BRND_RES_ID = "OFR_CAR_BRND_RES_ID";
	public static String OFR_CAR_BRND_RES_NM = "OFR_CAR_BRND_RES_NM";
	public static String OFR_CAR_BRND_RES_IMG = "OFR_CAR_BRND_RES_IMG";
	public static String OFR_CAR_BRND_RES_REC = "OFR_CAR_BRND_RES_REC";
	/**
	 * Car offer - list request
	 */
	public static String OFR_CAR_BRND_REQ_ID = "OFR_CAR_BRND_REQ_ID";
	/**
	 * Car offer - list response
	 */
	public static String OFR_CAR_LST_RES_ID = "OFR_CAR_LST_RES_ID";
	public static String OFR_CAR_LST_RES_TIT = "OFR_CAR_LST_RES_TIT";
	public static String OFR_CAR_LST_RES_SBT = "OFR_CAR_LST_RES_SBT";
	public static String OFR_CAR_LST_RES_IMG = "OFR_CAR_LST_RES_IMG";
	public static String OFR_CAR_LST_RES_BIMG = "OFR_CAR_LST_RES_BIMG";
	public static String OFR_CAR_LST_RES_BID = "OFR_CAR_LST_RES_BID";
	public static String OFR_CAR_LST_RES_LOC = "OFR_CAR_LST_RES_LOC";
	public static String OFR_CAR_LST_RES_VL = "OFR_CAR_LST_RES_VL";
	public static String OFR_CAR_LST_RES_REC = "OFR_CAR_LST_RES_REC";
	/**
	 * Car offer - details request
	 */
	public static String OFR_CAR_LST_REQ_ID = "OFR_CAR_LST_REQ_ID";
	public static String OFR_CAR_LST_REQ_BID = "OFR_CAR_LST_REQ_BID";
	/**
	 * Car offer - details response
	 */
	public static String OFR_CAR_DETS_RES_ID = "OFR_CAR_DETS_RES_ID";
	public static String OFR_CAR_DETS_RES_TIT = "OFR_CAR_DETS_RES_TIT";
	public static String OFR_CAR_DETS_RES_SBT = "OFR_CAR_DETS_RES_SBT";
	public static String OFR_CAR_DETS_RES_DET = "OFR_CAR_DETS_RES_DET";
	public static String OFR_CAR_DETS_RES_OFR = "OFR_CAR_DETS_RES_OFR";
	public static String OFR_CAR_DETS_RES_IMG = "OFR_CAR_DETS_RES_IMG";
	public static String OFR_CAR_DETS_RES_CONT1 = "OFR_CAR_DETS_RES_CONT1";
	public static String OFR_CAR_DETS_RES_CONT2 = "OFR_CAR_DETS_RES_CONT2";
	public static String OFR_CAR_DETS_RES_EML = "OFR_CAR_DETS_RES_EML";
	public static String OFR_CAR_DETS_RES_URL = "OFR_CAR_DETS_RES_URL";
	public static String OFR_CAR_DETS_RES_NM = "OFR_CAR_DETS_RES_NM";
	public static String OFR_CAR_DETS_RES_LOC = "OFR_CAR_DETS_RES_LOC";
	public static String OFR_CAR_DETS_RES_SDT = "OFR_CAR_DETS_RES_SDT";
	public static String OFR_CAR_DETS_RES_VL = "OFR_CAR_DETS_RES_VL";
	public static String OFR_CAR_DETS_RES_REC = "OFR_CAR_DETS_RES_REC";
	/**
	 * Other offer - list response
	 */
	public static String OFR_OTHR_LST_RES_ID = "OFR_OTHR_LST_RES_ID";
	public static String OFR_OTHR_LST_RES_TIT = "OFR_OTHR_LST_RES_TIT";
	public static String OFR_OTHR_LST_RES_SBT = "OFR_OTHR_LST_RES_SBT";
	public static String OFR_OTHR_LST_RES_IMG = "OFR_OTHR_LST_RES_IMG";
	public static String OFR_OTHR_LST_RES_LOC = "OFR_OTHR_LST_RES_LOC";
	public static String OFR_OTHR_LST_RES_VL = "OFR_OTHR_LST_RES_VL";
	/**
	 * Other offer - details request
	 */
	public static String OFR_OTHR_LST_REQ_ID = "OFR_OTHR_LST_REQ_ID";
	/**
	 * Other offer - details response
	 */
	public static String OFR_OTHR_DETS_RES_ID = "OFR_OTHR_DETS_RES_ID";
	public static String OFR_OTHR_DETS_RES_TIT = "OFR_OTHR_DETS_RES_TIT";
	public static String OFR_OTHR_DETS_RES_SBT = "OFR_OTHR_DETS_RES_SBT";
	public static String OFR_OTHR_DETS_RES_DET = "OFR_OTHR_DETS_RES_DET";
	public static String OFR_OTHR_DETS_RES_OFR = "OFR_OTHR_DETS_RES_OFR";
	public static String OFR_OTHR_DETS_RES_IMG = "OFR_OTHR_DETS_RES_IMG";
	public static String OFR_OTHR_DETS_RES_CONT1 = "OFR_OTHR_DETS_RES_CONT1";
	public static String OFR_OTHR_DETS_RES_CONT2 = "OFR_OTHR_DETS_RES_CONT2";
	public static String OFR_OTHR_DETS_RES_EML = "OFR_OTHR_DETS_RES_EML";
	public static String OFR_OTHR_DETS_RES_URL = "OFR_OTHR_DETS_RES_URL";
	public static String OFR_OTHR_DETS_RES_NM = "OFR_OTHR_DETS_RES_NM";
	public static String OFR_OTHR_DETS_RES_LOC = "OFR_OTHR_DETS_RES_LOC";
	public static String OFR_OTHR_DETS_RES_SDT = "OFR_OTHR_DETS_RES_SDT";
	public static String OFR_OTHR_DETS_RES_VL = "OFR_OTHR_DETS_RES_VL";
	public static String OFR_OTHR_DETS_RES_REC = "OFR_OTHR_DETS_RES_REC";
	/**
	 * Personal Detail - request
	 */
	public static String PER_DETS_REQ_EMP_ID = "PER_DETS_REQ_EMP_ID"; // Personal
																		// details
																		// request
	/**
	 * Personal Detail - response
	 */
	public static String PER_DETS_RES_EMP_ID = "PER_DETS_RES_EMP_ID";
	public static String PER_DETS_RES_EMP_NM = "PER_DETS_RES_EMP_NM";
	public static String PER_DETS_RES_ACC_ID = "PER_DETS_RES_ACC_ID";
	public static String PER_DETS_RES_BLD_GP = "PER_DETS_RES_BLD_GP";
	public static String PER_DETS_RES_MDL_POL_NUM = "PER_DETS_RES_MDL_POL_NUM";
	public static String PER_DETS_RES_EMG_CON = "PER_DETS_RES_EMG_CON";
	public static String PER_DETS_RES_EMG_NM = "PER_DETS_RES_EMG_NM";
	public static String PER_DETS_RES_EMG_CONT = "PER_DETS_RES_EMG_CONT";
	public static String PER_DETS_RES_EMG_CONT_PRI = "PER_DETS_RES_EMG_CONT_PRI";
	public static String PER_DETS_RES_NAT_ID = "PER_DETS_RES_NAT_ID";
	public static String PER_DETS_RES_DEP_NAME = "PER_DETS_RES_DEP_NAME";
	public static String PER_DETS_RES_ALT_NO = "PER_DETS_RES_ALT_NO";
	public static String PER_DETS_RES_E_MAIL = "PER_DETS_RES_E_MAIL";
	public static String PER_DETS_RES_COST_CEN = "PER_DETS_RES_COST_CEN";
	public static String LV_P_CON_REQ_EMP_ID = "LV_P_CON_REQ_EMP_ID";
	public static String LV_P_CON_RES_EMP_ID = "LV_P_CON_RES_EMP_ID";
	public static String LV_P_CON_RES_CON_TOT = "LV_P_CON_RES_CON_TOT";
	public static String MS_P_CON_REQ_EMP_ID = "MS_P_CON_REQ_EMP_ID";
	public static String MS_P_CON_RES_EMP_ID = "MS_P_CON_RES_EMP_ID";
	public static String MS_P_CON_RES_CON_TOT = "MS_P_CON_RES_CON_TOT";
	public static String EXP_P_CON_REQ_EMP_ID = "EXP_P_CON_REQ_EMP_ID";
	public static String EXP_P_CON_RES_EMP_ID = "EXP_P_CON_RES_EMP_ID";
	public static String EXP_P_CON_RES_CON_TOT = "EXP_P_CON_RES_CON_TOT";

	/*
	 * I am interested request
	 */
	public static String OFR_INTR_REQ_EMP_ID = "OFR_INTR_REQ_EMP_ID";
	public static String OFR_INTR_REQ_OFR_ID = "OFR_INTR_REQ_OFR_ID";
	public static String OFR_INTR_REQ_EMP_IP = "OFR_INTR_REQ_EMP_IP";
	public static String OFR_INTR_REQ_EMP_OS = "OFR_INTR_REQ_EMP_OS";
	/*
	 * I am interested response
	 */
	public static String OFR_INTR_RES_CODE = "OFR_INTR_REQ_CODE";
	public static String OFR_INTR_RES_MSG = "OFR_INTR_REQ_MSG";
	public static String ADT_CD_LOG_EROR = "LOG_EROR";
	/**
	 * Personal details Update form submit Parameters request
	 */
	public static String PER_DETS_UPDT_REQ_BLD_GP = "PER_DETS_UPDT_REQ_BLD_GP";
	public static String PER_DETS_UPDT_REQ_EMG_CON_NUM = "PER_DETS_UPDT_REQ_EMG_CON_NUM";
	public static String PER_DETS_UPDT_REQ_EMG_CON_NM = "PER_DETS_UPDT_REQ_EMG_CON_NM";
	public static String PER_DETS_UPDT_REQ_ALT_NUM = "PER_DETS_UPDT_REQ_ALT_NUM";
	public static String PER_DETS_UPDT_REQ_PER_E_MAIL = "PER_DETS_UPDT_REQ_PER_E_MAIL";
	public static String PER_DETS_UPDT_REQ_PER_COST_CNTR = "PER_DETS_UPDT_REQ_PER_COST_CNTR";
	public static String EXP_PSBMT_RES_APR_AMT = "EXP_PSBMT_RES_APR_AMT";
	public static String EXP_PSBMT_RES_CLM_AMT = "EXP_PSBMT_RES_CLM_AMT";
	public static String PER_DETS_UPDT_REQ_EMP_ID = "PER_DETS_UPDT_REQ_EMP_ID";
	public static String PER_DETS_UPDT_REQ_PER_SBMT = "PER_DETS_UPDT_REQ_PER_SBMT";
	public static String PER_DETS_UPDT_REQ_PER_REL = "PER_DETS_UPDT_REQ_PER_REL";
	public static String PER_DETS_RES_PER_REL = "PER_DETS_RES_PER_REL";

	/* Petrol */
	/* Unlock user id */
	public static String UNLK_ID_REQ_USER_ID = "UNLK_ID_REQ_USER_ID";
	public static String UNLK_ID_REQ_EMP_ID = "UNLK_ID_REQ_EMP_ID";
	public static String UNLK_ID_REQ_EMP_NM = "UNLK_ID_REQ_EMP_NM";
	public static String UNLK_ID_REQ_EMP_NOTE = "UNLK_ID_REQ_EMP_NOTE";
	// Unlock user id request
	public static String UNLK_ID_RES_SR_NUM = "UNLK_ID_RES_SR_NUM";
	public static String UNLK_ID_RES_SR_DUE_DT = "UNLK_ID_RES_SR_DUE_DT";
	// Multiple leave approve reject
	public static String LV_M_AP_REQ_EMP_ID = "LV_M_AP_REQ_EMP_ID"; // approvals
																	// emp id
	public static String LV_M_AP_REQ_TRXN_ID = "LV_M_AP_REQ_TRXN_ID"; // Transaction
																		// id
	public static String LV_M_AP_REQ_CMT = "LV_M_AP_REQ_CMT"; // emp id by comma
																// separated
	public static String LV_M_AP_REQ_SV = "LV_M_AP_REQ_SV"; // Y
	public static String LV_M_AP_REQ_TL_APP = "LV_M_AP_REQ_TL_APP"; // A or D
	// Multiple leave approve reject
	public static String MS_M_AP_REQ_EMP_ID = "MS_M_AP_REQ_EMP_ID"; // aprovals
																	// user id
	public static String MS_M_AP_REQ_SV = "MS_M_AP_REQ_SV"; // "Y"
	public static String MS_M_AP_REQ_CMT = "MS_M_AP_REQ_CMT"; // "Duration with
																// comma
																// separated"
	public static String MS_M_AP_REQ_TL_APP = "MS_M_AP_REQ_TL_APP"; // "A" or
																	// "D"
	public static String MS_M_AP_REQ_ID_VERF = "MS_M_AP_REQ_ID_VERF"; // "emp
																		// ids
																		// with
																		// comma
																		// separated"
	// Late punch details request parameter
	public static String LATE_PNCH_REQ_EMP_ID = "LATE_PNCH_REQ_EMP_ID"; // manager
																		// id
	// Late punch details response parameter
	public static String LATE_PNCH_RES_EMP_ID = "LATE_PNCH_RES_EMP_ID"; // employee
																		// id
	public static String LATE_PNCH_RES_EMP_NM = "LATE_PNCH_RES_EMP_NM"; // employee
																		// name
	public static String LATE_PNCH_RES_DUR = "LATE_PNCH_RES_DUR"; // duration -
																	// yyyy-mm-dd
	public static String LATE_PNCH_RES_PNCH_TM = "LATE_PNCH_RES_PNCH_TM"; // hh:mm
	public static String LATE_PNCH_RES_SPRVR_ID = "LATE_PNCH_RES_SPRVR_ID"; // Manager
																			// id
	public static String LATE_PNCH_RES_CMTS = "LATE_PNCH_RES_CMTS"; // comment
	public static String LATE_PNCH_DAT_RES_TM = "LATE_PNCH_DAT_RES_TM";
	// Late punch approve reject request parameter
	public static String LATE_PNCH_AP_REQ_EMP_ID = "LATE_PNCH_AP_REQ_EMP_ID";
	public static String LATE_PNCH_AP_REQ_SV = "LATE_PNCH_AP_REQ_SV";
	public static String LATE_PNCH_AP_REQ_CMT = "LATE_PNCH_AP_REQ_CMT";
	public static String LATE_PNCH_AP_REQ_TL_APP = "LATE_PNCH_AP_REQ_TL_APP";
	public static String LATE_PNCH_AP_REQ_ID_VERF = "LATE_PNCH_AP_REQ_ID_VERF";
	// Late punch blank dates response parameter
	public static String LATE_PNCH_DAT_RES_DUR = "LATE_PNCH_DAT_RES_DUR";
	// Late punch apply request parameter
	public static String LATE_PNCH_SUB_REQ_DUR = "LATE_PNCH_SUB_REQ_DUR";
	public static String LATE_PNCH_SUB_REQ_REMRK = "LATE_PNCH_SUB_REQ_REMRK";
	// Late punch apply response parameter
	public static String LATE_PNCH_SUB_RSP_NTFCN = "LATE_PNCH_SUB_RSP_NTFCN";
	public static String LATE_PNCH_SUB_RSP_DET = "LATE_PNCH_SUB_RSP_DET";
	// Late punch history details request parameter
	public static String LATE_PNCH_HIST_REQ_EMP_ID = "LATE_PNCH_HIST_REQ_EMP_ID";
	// Late punch history details response parameter
	public static String LATE_PNCH_HIST_RES_DUR = "LATE_PNCH_HIST_RES_DUR";
	public static String LATE_PNCH_HIST_RES_PNCH_TM = "LATE_PNCH_HIST_RES_PNCH_TM";
	public static String LATE_PNCH_HIST_RES_SUP_ID = "LATE_PNCH_HIST_RES_SUP_ID";
	public static String LATE_PNCH_HIST_RES_WF_STS = "LATE_PNCH_HIST_RES_WF_STS";
	public static String LATE_PNCH_HIST_RES_LT_PNCH = "LATE_PNCH_HIST_RES_LT_PNCH";
	public static String LATE_PNCH_HIST_RES_REMRK = "LATE_PNCH_HIST_RES_REMRK";
	public static String LATE_PNCH_HIST_RES_EMP_NM = "LATE_PNCH_HIST_RES_EMP_NM";
	public static String LATE_PNCH_HIST_RES_SUP_NM = "LATE_PNCH_HIST_RES_SUP_NM";
	// Late punch count request
	public static String LP_P_CON_REQ_EMP_ID = "LP_P_CON_REQ_EMP_ID";
	// Late punch details response
	public static String LP_P_CON_RES_EMP_ID = "LP_P_CON_RES_EMP_ID";
	public static String LP_P_CON_RES_CON_TOT = "LP_P_CON_RES_CON_TOT";
	// 28/5/15
	public static String EXP_DET_RES_SRC = "EXP_DET_RES_SRC";
	public static String EXP_PND_APP_RES_SRC = "EXP_PND_APP_RES_SRC";
	public static String SRV_NM_EXP_CHK_BLK = "SRV_NM_EXP_CHK_BLK";
	// public static String EXP_SBMT_REQ_SESS = "EXP_SBMT_REQ_SESS";
	public static String SRV_NM_EXP_SBMT_SAVD = "SRV_NM_EXP_SBMT_SAVD";
	public static String EXP_SBMTSAVED_REQ_EMPID = "EXP_SBMTSAVED_REQ_EMPID";
	public static String EXP_SBMTSAVED_REQ_FORM = "EXP_SBMTSAVED_REQ_FORM";
	// Register request
	public static String LGN_REQ_REG_EMP_ID = "LGN_REQ_REG_EMP_ID";
	public static String LGN_REQ_REG_EMP_DVCE_ID = "LGN_REQ_REG_EMP_DVCE_ID";
	public static String LGN_REQ_REG_EMP_NTFN_ID = "LGN_REQ_REG_EMP_NTFN_ID";
	public static String LGN_REQ_REG_EMP_DVCE_TYP = "LGN_REQ_REG_EMP_DVCE_TYP";
	// OTP request
	public static String LGN_REQ_OTP_EMP_ID = "LGN_REQ_OTP_EMP_ID";
	public static String LGN_REQ_OTP_EMP_OTP = "LGN_REQ_OTP_EMP_OTP";
	public static String LGN_REQ_OTP_EMP_DVCE_ID = "LGN_REQ_OTP_EMP_DVCE_ID";
	public static String LGN_REQ_OTP_EMP_NTFN_ID = "LGN_REQ_OTP_EMP_NTFN_ID";
	public static String LGN_REQ_OTP_EMP_DVCE_TYP = "LGN_REQ_OTP_EMP_DVCE_TYP";
	// MPIN request
	public static String LGN_REQ_MPIN_EMP_ID = "LGN_REQ_MPIN_EMP_ID";
	public static String LGN_REQ_MPIN_EMP_MPIN = "LGN_REQ_MPIN_EMP_MPIN";
	public static String LGN_REQ_MPIN_EMP_DVCE_ID = "LGN_REQ_MPIN_EMP_DVCE_ID";
	public static String LGN_REQ_MPIN_EMP_NTFN_ID = "LGN_REQ_MPIN_EMP_NTFN_ID";
	public static String LGN_REQ_MPIN_EMP_DVCE_TYP = "LGN_REQ_MPIN_EMP_DVCE_TYP";
	public static String LGN_REQ_MPIN_EMP_DVCE_MDL = "LGN_REQ_MPIN_EMP_DVCE_MDL";
	// MPIN responce
	public static String LGN_RSP_MPIN_EMP_ID = "LGN_RSP_MPIN_EMP_ID"; // response
																		// employee
																		// id
	public static String LGN_RSP_MPIN_GRADE = "LGN_RSP_MPIN_GRADE"; // user
																	// grade
	public static String LGN_RSP_MPIN_EMP_NM = "LGN_RSP_MPIN_EMP_NM"; // user
																		// full
																		// name
	public static String LGN_RSP_MPIN_EMP_TYP = "LGN_RSP_MPIN_EMP_TYP"; // Employee
																		// type
																		// M-
																		// manager
																		// or E-
																		// Employee
	public static String LGN_RSP_MPIN_EMP_MPIN = "LGN_RSP_MPIN_EMP_MPIN"; // Employee
																			// Encrypted
																			// MPIN
	public static String LGN_RSP_MPIN_EMP_REG = "LGN_RSP_MPIN_EMP_REG"; // Employee
																		// Register
	public static String LGN_RSP_MPIN_EMP_LOC = "LGN_RSP_MPIN_EMP_LOC"; // Employee
																		// Location
	public static String LGN_RSP_MPIN_EMP_NODE = "LGN_RSP_MPIN_EMP_NODE"; // Employee
																			// Node

	// Auto Login Request
	public static String LGN_REQ_AUTO_EMP_ID = "LGN_REQ_AUTO_EMP_ID";
	public static String LGN_REQ_AUTO_EMP_MPIN = "LGN_REQ_AUTO_EMP_MPIN";
	public static String LGN_REQ_AUTO_EMP_DVCE_ID = "LGN_REQ_AUTO_EMP_DVCE_ID";
	public static String LGN_REQ_AUTO_EMP_NTFN_ID = "LGN_REQ_AUTO_EMP_NTFN_ID";
	public static String LGN_REQ_AUTO_EMP_DVCE_TYP = "LGN_REQ_AUTO_EMP_DVCE_TYP";
	public static String LGN_REQ_AUTO_EMP_BIO = "LGN_REQ_AUTO_EMP_BIO";

	// Auto Login Responce
	public static String LGN_RSP_AUTO_EMP_ID = "LGN_RSP_AUTO_EMP_ID"; // response
																		// employee
																		// id
	public static String LGN_RSP_AUTO_GRADE = "LGN_RSP_AUTO_GRADE"; // user
																	// grade
	public static String LGN_RSP_AUTO_EMP_NM = "LGN_RSP_AUTO_EMP_NM"; // user
																		// full
																		// name
	public static String LGN_RSP_AUTO_EMP_TYP = "LGN_RSP_AUTO_EMP_TYP"; // Employee
																		// type
																		// M-
																		// manager
																		// or E-
																		// Employee
	public static String LGN_RSP_AUTO_EMP_MPIN = "LGN_RSP_AUTO_EMP_MPIN"; // Employee
																			// Encrypted
																			// MPIN
	public static String LGN_RSP_AUTO_EMP_REG = "LGN_RSP_AUTO_EMP_REG"; // Employee
																		// Register
	public static String LGN_RSP_AUTO_EMP_LOC = "LGN_RSP_AUTO_EMP_LOC"; // Employee
																		// Location
	public static String LGN_RSP_AUTO_EMP_NODE = "LGN_RSP_AUTO_EMP_NODE"; // Employee
																			// Node

	// Reset MPIN Request
	public static String RESET_MPIN_REQ_EMP_ID = "RESET_MPIN_REQ_EMP_ID"; // Employee
																			// ID
	public static String RESET_MPIN_REQ_EMP_PSWD = "RESET_MPIN_REQ_EMP_PSWD"; // Employee
																				// ID
	public static String RESET_MPIN_REQ_EMP_MPIN = "RESET_MPIN_REQ_EMP_MPIN"; // Employee
																				// MPIN
	public static String RESET_MPIN_REQ_EMP_DVCE_ID = "RESET_MPIN_REQ_EMP_DVCE_ID"; // Device
																					// ID
	public static String RESET_MPIN_REQ_EMP_DVCE_TYP = "RESET_MPIN_REQ_EMP_DVCE_TYP"; // Device
																						// Type
	public static String RESET_MPIN_REQ_EMP_NTFN_ID = "RESET_MPIN_REQ_EMP_NTFN_ID"; // Notification
																					// ID
	// Reset MPIN Responce
	public static String RESET_MPIN_RSP_EMP_ID = "RESET_MOIN_RSP_EMP_ID"; // Employee
																			// ID
	public static String RESET_MPIN_RSP_EMP_DVCE_ID = "RESET_MOIN_RSP_EMP_DVCE_ID"; // Device
																					// ID
	public static String RESET_MOIN_RSP_EMP_DVCE_TYP = "RESET_MOIN_RSP_EMP_DVCE_TYP"; // Device
																						// Type
	public static String RESET_MPIN_RSP_EMP_NTFN_ID = "RESET_MOIN_RSP_EMP_NTFN_ID"; // Notification
																					// ID
	public static String RESET_MPIN_RSP_EMP_MPIN = "RESET_MPIN_RSP_EMP_MPIN"; // Employee
																				// MPIN
	// Register device List Request
	public static String REG_DVC_LST_REQ_EMP_ID = "REG_DVC_LST_REQ_EMP_ID";
	// Register device List Responce
	public static String REG_DVC_LST_RSP_EMP_ID = "REG_DVC_LST_RSP_EMP_ID";
	public static String REG_DVC_LST_RSP_EMP_DVCE_ID = "REG_DVC_LST_RSP_EMP_DVCE_ID";
	public static String REG_DVC_LST_RSP_EMP_DVCE_TYP = "REG_DVC_LST_RSP_EMP_DVCE_TYP";
	public static String REG_DVC_LST_RSP_EMP_DVCE_MDL = "REG_DVC_LST_RSP_EMP_DVCE_MDL";
	public static String REG_DVC_LST_RSP_EMP_NTFN_ID = "REG_DVC_LST_RSP_EMP_NTFN_ID";
	// De-register Request
	public static String DE_REG_REQ_EMP_ID = "DE_REG_REQ_EMP_ID";
	public static String DE_REG_REQ_EMP_DVCE_ID = "DE_REG_REQ_EMP_DVCE_ID";
	public static String DE_REG_REQ_EMP_DVCE_TYP = "DE_REG_REQ_EMP_DVCE_TYP";
	public static String DE_REG_REQ_EMP_NTFN_ID = "DE_REG_REQ_EMP_NTFN_ID";
	// Menu MPIN Request
	public static String MPIN_VRFY_REQ_EMP_ID = "MPIN_VRFY_REQ_EMP_ID"; // Employee
																		// ID
	public static String MPIN_VRFY_REQ_EMP_MPIN = "MPIN_VRFY_REQ_EMP_MPIN"; // Employee
																			// MPIN
	public static String MPIN_VRFY_REQ_EMP_DVCE_ID = "MPIN_VRFY_REQ_EMP_DVCE_ID"; // Device
																					// ID
	public static String MPIN_VRFY_REQ_EMP_DVCE_TYP = "MPIN_VRFY_REQ_EMP_DVCE_TYP"; // Device
																					// Type
	public static String MPIN_VRFY_REQ_EMP_NTFN_ID = "MPIN_VRFY_REQ_EMP_NTFN_ID"; // Notification
																					// ID
	// ERApp interation (Apulse) start
	public static String ER_RES_AUTO_CMPLT_SR_NO = "ER_RES_AUTO_CMPLT_SR_NO"; // Serial
																				// number
	public static String ER_RES_AUTO_CMPLT_CONT = "ER_RES_AUTO_CMPLT_CONT"; // content
	public static String ER_RES_SERCH_RSLT_SR_NO = "ER_RES_SERCH_RSLT_SR_NO"; // Serial
																				// number
	public static String ER_RES_SERCH_RSLT_EMP_ID = "ER_RES_SERCH_RSLT_EMP_ID"; // EMP
																				// ID
	public static String ER_RES_SERCH_RSLT_NAME = "ER_RES_SERCH_RSLT_NAME"; // NAME
	public static String ER_RES_SERCH_RSLT_LOC = "ER_RES_SERCH_RSLT_LOC"; // Location
	public static String ER_RES_SERCH_RSLT_DPT = "ER_RES_SERCH_RSLT_DPT"; // Department
	public static String ER_RES_SERCH_RSLT_GRD = "ER_RES_SERCH_RSLT_GRD"; // GRADE
	public static String ER_RES_SERCH_RSLT_MAIN_GRP = "ER_RES_SERCH_RSLT_MAIN_GRP"; // MAIN
																					// GROUP
	public static String ER_RES_SERCH_RSLT_JOB_TIT = "ER_RES_SERCH_RSLT_JOB_TIT"; // Job
																					// Title
	public static String ER_RES_SERCH_RSLT_MOB = "ER_RES_SERCH_RSLT_MOB"; // mobile
	public static String ER_RES_SERCH_RESULT_EMAIL = "ER_RES_SERCH_RESULT_EMAIL"; // mobile
	public static String ER_RES_DTL_SRV_NAME = "ER_RES_DTL_SRV_NAME"; // Name
	public static String ER_RES_DTL_SRV_EMP_ID = "ER_RES_DTL_SRV_EMP_ID"; // Emp
																			// Id
	public static String ER_RES_DTL_SRV_GRD = "ER_RES_DTL_SRV_GRD"; // Grade
	public static String ER_RES_DTL_SRV_DSGN = "ER_RES_DTL_SRV_DSGN"; // Designation
	public static String ER_RES_DTL_SRV_MAIN_GRP = "ER_RES_DTL_SRV_MAIN_GRP"; // Main
																				// Group
	public static String ER_RES_DTL_SRV_SUB_GRP = "ER_RES_DTL_SRV_SUB_GRP"; // Sub
																			// Group
	public static String ER_RES_DTL_SRV_RTP_AUTH = "ER_RES_DTL_SRV_RTP_AUTH"; // Reporting
																				// Authority
																				// Name
	public static String ER_RES_DTL_SRV_DEPT = "ER_RES_DTL_SRV_DEPT"; // Department
	public static String ER_RES_DTL_SRV_LOC = "ER_RES_DTL_SRV_LOC"; // Location
	public static String ER_RES_DTL_SRV_OFF_STS = "ER_RES_DTL_SRV_OFF_STS"; // Office
																			// State
	public static String ER_RES_DTL_SRV_EMP_STUS = "ER_RES_DTL_SRV_EMP_STUS"; // Employee
																				// Status
	public static String ER_RES_DTL_SRV_DOB = "ER_RES_DTL_SRV_DOB"; // DOB
	public static String ER_RES_DTL_SRV_DOJ = "ER_RES_DTL_SRV_DOJ"; // DOJ
	public static String ER_RES_DTL_SRV_DOW = "ER_RES_DTL_SRV_DOW"; // Work Date
	public static String ER_RES_DTL_SRV_ADD = "ER_RES_DTL_SRV_ADD"; // Address
	public static String ER_RES_DTL_SRV_EMP_RESN = "ER_RES_DTL_SRV_RESN"; // Employer
																			// Reason
	public static String ER_RES_DTL_SRV_PH_NO = "ER_RES_DTL_SRV_PH_NO"; // Phone
																		// Number
	public static String ER_RES_DTL_SRV_CONT_NO = "ER_RES_DTL_SRV_CONT_NO"; // Contact
																			// Number
	public static String ER_RES_DTL_SRV_EMG_CONT_NAME = "ER_RES_DTL_SRV_EMG_CONT_NAME"; // Contatc
																						// Name
	public static String ER_RES_DTL_SRV_MAR_STUS = "ER_RES_DTL_SRV_MAR_STUS"; // Mar
																				// Status
	public static String ER_RES_DTL_SRV_ORG_CMP = "ER_RES_DTL_SRV_ORG_CMP"; // Org
																			// Company
	public static String ER_RES_DTL_SRV_BLD_GRP = "ER_RES_DTL_SRV_BLD_GRP"; // Bld
																			// Group
	public static String ER_EDU_DTL_SRV_QULI = "ER_EDU_DTL_SRV_QULI"; // Qualification
	public static String ER_EDU_DTL_SRV_SUB = "ER_EDU_DTL_SRV_SUB"; // Subject
	public static String ER_EDU_DTL_SRV_COLG = "ER_EDU_DTL_SRV_COLG"; // College
	public static String ER_EDU_DTL_SRV_YER = "ER_EDU_DTL_SRV_YER"; // Year
	public static String ER_EDU_DTL_SRV_FULTIM_PRTTIM = "ER_EDU_DTL_SRV_FULTIM_PRTTIM"; // Full
																						// Time
																						// Part
																						// Time
	public static String ER_PRV_DTL_SRV_ORG = "ER_PRV_DTL_SRV_ORG"; // Orgnisation
	public static String ER_PRV_DTL_SRV_FRM_DT = "ER_PRV_DTL_SRV_FRM_DT"; // From
																			// date
	public static String ER_PRV_DTL_SRV_TO_DT = "ER_PRV_DTL_SRV_TO_DT"; // to
																		// date
	public static String ER_PRV_DTL_SRV_POS = "ER_PRV_DTL_SRV_POS"; // Position
	public static String ER_RTING_DTL_SRV_EMP_ID = "ER_RTING_DTL_SRV_EMP_ID"; // Emp
																				// ID
	public static String ER_RTING_DTL_SRV_RTING = "ER_RTING_DTL_SRV_RTING"; // Rating
	public static String ER_RTING_DTL_SRV_YER = "ER_RTING_DTL_SRV_YER"; // Year
	public static String ER_RTING_DTL_SRV_TPE = "ER_RTING_DTL_SRV_TPE"; // Type
	public static String ER_CRIRP_DTL_SRV_EFF_DT = "ER_CRIRP_DTL_SRV_EFF_DT"; // Effective
																				// Date
	public static String ER_CRIRP_DTL_SRV_GRD = "ER_CRIRP_DTL_SRV_GRD"; // Grade
	public static String ER_TRNS_DTL_SRV_PEPT_BRAN = "ER_TRNS_DTL_SRV_PEPT_BRAN"; // Department
																					// Branch
	public static String ER_TRNS_DTL_SRV_EF_DT = "ER_TRNS_DTL_SRV_EF_DT"; // Effective
																			// Date
	public static String ER_TRNS_DTL_SRV_DESG = "ER_TRNS_DTL_SRV_DESG"; // Designation
	public static String ER_TRNS_DTL_SRV_LOC = "ER_TRNS_DTL_SRV_LOC"; // Location
	public static String ER_FAMI_DTL_SRV_DOB = "ER_FAMI_DTL_SRV_DOB"; // DOB
	public static String ER_TRNS_DTL_SRV_FNAME = "ER_TRNS_DTL_SRV_FNAME"; // First
																			// Name
	public static String ER_TRNS_DTL_SRV_OCCU = "ER_TRNS_DTL_SRV_OCCU"; // Occupation
	public static String ER_TRNS_DTL_SRV_REL = "ER_TRNS_DTL_SRV_REL"; // relation
	public static String ER_TRNS_DTL_SRV_EMP_MARRI = "ER_TRNS_DTL_SRV_EMP_MARRI"; // Emp
																					// Marriage
	public static String ER_TRNS_DTL_SRV_FAM_LIST = "ER_TRNS_DTL_SRV_FAM_LIST"; // Family
																				// Member
																				// Detail
																				// List
	public static String ER_USRV_DTL_SRV_AC_DT = "ER_USRV_DTL_SRV_AC_DT"; // Action
																			// Date
	public static String ER_USRV_DTL_SRV_DIS_TYP = "ER_USRV_DTL_SRV_DIS_TYP"; // Disciplinary
																				// Type
	public static String ER_USRV_DTL_SRV_DOD = "ER_USRV_DTL_SRV_DOD"; // Date of
																		// Detaction
	public static String ER_USRV_DTL_SRV_ROD = "ER_USRV_DTL_SRV_ROD"; // Reported
																		// Date
	public static String ER_USRV_DTL_SRV_FINL_RESO = "ER_USRV_DTL_SRV_FINL_RESO"; // Final
																					// Resolution
	public static String ER_USRV_DTL_SRV_GOL = "ER_USRV_DTL_SRV_GOL"; // Gist Of
																		// Lapses
	public static String ER_EBOD_DTL_SRV_CBY = "ER_EBOD_DTL_SRV_CBY"; // Created
																		// BY
	public static String ER_EBOD_DTL_SRV_CDT = "ER_EBOD_DTL_SRV_CDT"; // created
																		// Date
	public static String ER_EBOD_DTL_SRV_NOTS = "ER_EBOD_DTL_SRV_NOTS"; // Nots
	public static String ER_Auto = "ER_Auto"; // Auto Complete
	public static String ER_Srch_Srv = "ER_Srch_Srv"; // search service
	public static String ER_User_Dtl = "ER_User_Dtl"; // user deteail
	public static String ER_Edu_Dtl = "ER_Edu_Dtl"; // education deteail
	public static String ER_Pre_Ex_Dtl = "ER_Pre_Ex_Dtl"; // pre deteail
	public static String ER_Rating_Dtl = "ER_Rating_Dtl"; // Rating deteail
	public static String ER_Car_Pro_Dtl = "ER_Car_Pro_Dtl"; // Carear Por
															// deteail
	public static String ER_Tra_Dtl = "ER_Tra_Dtl"; // transfer deteail
	public static String ER_Famly_Dtl = "ER_Famly_Dtl"; // Family deteail
	public static String ER_Srv_Dtl = "ER_Srv_Dtl"; // service deteail
	public static String ER_Edbo_Dtl = "ER_Edbo_Dtl"; // Edbo deteail
	// ERApp interation (Apulse) End

	// PMS Token

	public static String PMS_RES_EMP_ID = "PMS_RES_EMP_ID"; // PMS emplid
	public static String PMS_RES_EMP_TKN = "PMS_RES_EMP_TKN"; // PMS Token

	// Investment Request Parameters
	public static String INVST_REQ_EMP_ID = "INVST_REQ_EMP_ID";
	public static String INVST_REQ_TX_YR = "INVST_REQ_TX_YR";
	public static String INVST_REQ_UPL_TYPE = "INVST_REQ_UPL_TYPE";

	// INVESTMENT Response
	public static String INVST_RES_T05_INV_ITEM = "INVST_RES_T05_INV_ITEM";// investment
																			// item
	public static String INVST_RES_T05_INV_ITEM_DESC = "INVST_RES_T05_INV_ITEM_DESC";// investment
																						// item
																						// description
	public static String INVST_RES_SR_NO = "INVST_RES_SR_NO";// seriel number
	public static String INVST_RES_H01_EMP_NUM = "INVST_RES_H01_EMP_NUM";
	public static String INVST_RES_T06_INV_AMOUNT = "INVST_RES_T06_INV_AMOUNT";
	public static String INVST_RES_AMT_CNSRD = "INVST_RES_AMT_CNSRD"; // Amount
																		// Considered
	public static String INVST_RES_REVSD_AMT = "INVST_RES_REVSD_AMT"; // Revised
																		// Amount
	public static String INVST_RES_FINL_AMT = "INVST_RES_FINL_AMT"; // Final
																	// Amount
	public static String INVST_RES_RMRK = "INVST_RES_RMRK"; // Remark
	public static String INVST_RES_UP_FL_NM = "INVST_RES_UP_FL_NM";
	public static String INVST_RES_T04_INV_FLAG = "INVST_RES_T04_INV_FLAG";
	public static String INVST_RES_T01_TAX_CAL_CODE = "INVST_RES_T01_TAX_CAL_CODE";
	public static String INVST_RES_CAT_ID = "INVST_RES_CAT_ID"; // CATID
	public static String INVST_RES_CAT_TYP = "INVST_RES_CAT_TYP"; // CATTYPE
	public static String INVST_RES_TX_YR = "INVST_RES_TX_YR"; // Tax Year
	public static String INVST_RES_PL_ORG = "INVST_RES_PL_ORG"; // Place of
																// Origin
	public static String INVST_RES_DSTN = "INVST_RES_DSTN"; // Destination
	public static String INVST_RES_FRM_DTE = "INVST_RES_FRM_DTE"; // From Date
	public static String INVST_RES_TO_DTE = "INVST_RES_TO_DTE"; // To Date
	public static String INVST_RES_INV_CD = "INVST_RES_INV_CD"; // Invoice Code
	public static String INVST_RES_INV_DESC = "INVST_RES_INV_DESC"; // Invoice
																	// Description
	public static String INVST_RES_EMP_AMT = "INVST_RES_EMP_AMT"; // Employee
																	// Amount
	public static String INVST_RES_AMT = "INVST_RES_AMT"; // Amount
	public static String INVST_RES_ANUL_LTB_A = "INVST_RES_ANUL_LTB_A"; // AnnualLetable_A
	public static String INVST_RES_MNCP_TX_PD_B = "INVST_RES_MNCP_TX_PD_B"; // MunicipalTaxPaid_B
	public static String INVST_RES_STRD_DDCT_D = "INVST_RES_STRD_DDCT_D"; // StandardDeduction_D
	public static String INVST_RES_INTRST_BRWD_CP_E = "INVST_RES_INTRST_BRWD_CP_E"; // InterestOnBorrowedCap_E
	public static String INVST_RES_PRP_ADDR = "INVST_RES_PRP_ADDR"; // Prop
																	// Address
	public static String INVST_RES_RENT_LST = "INVST_RES_RENT_LST"; // rent list
	public static String INVST_RES_PAN_LST = "INVST_RES_PAN_LST"; // pan list
	public static String INVST_RES_ID = "INVST_RES_ID"; // id
	public static String INVST_RES_LNDR_NM = "INVST_RES_LNDR_NM"; // Lender Name
	public static String INVST_RES_LNDR_ADD = "INVST_RES_LNDR_ADD"; // Lender
																	// ADD
	public static String INVST_RES_LNDR_PAN = "INVST_RES_LNDR_PAN"; // Lender
																	// Pan
	public static String INVST_RES_PRPTY_TYP = "INVST_RES_PRPTY_TYP"; // Property
																		// Type
	public static String INVST_RES_INVST_CD = "INVST_RES_INVST_CD"; // Investment
																	// code
	public static String INVST_RES_CHECK_COUNT = "INVST_RES_CHECK_COUNT"; // CHECK
																			// COUNT
	public static String INVST_RES_RNT_MNT_DESC = "INVST_RES_RNT_MNT_DESC"; // Rent
																			// Month
																			// Description
	public static String INVST_RES_RNT1 = "INVST_RES_RNT1"; // Rent1
	public static String INVST_RES_EMP_ID = "INVST_RES_EMP_ID"; // Employee Id
	public static String INVST_RES_PAN_NO = "INVST_RES_PAN_NO"; // Pan No.
	public static String INVST_RES_ADRS = "INVST_RES_ADRS"; // Address
	public static String INVST_RES_SUB_DT = "INVST_RES_SUB_DT"; // INVST_RES_SUB_DT
	public static String INVST_RES_STATE = "INVST_RES_STATE"; // State
	public static String INVST_RES_CITY = "INVST_RES_CITY"; // City
	public static String INVST_RES_PIN_CD = "INVST_RES_PIN_CD"; // Pin Code
	public static String INVST_RES_CRTD_DT = "INVST_RES_CRTD_DT"; // Created
																	// Date
	public static String INVST_RES_CRTD_BY = "INVST_RES_CRTD_BY"; // Created By
	public static String INVST_RES_UPTD_DT = "INVST_RES_UPTD_DT"; // Updated
																	// Date
	public static String INVST_RES_UPTD_BY = "INVST_RES_UPTD_BY"; // Updated By
	public static String INVST_RES_STATUS = "INVST_RES_STATUS"; // Status
	public static String INVST_RES_LNDRD_NME = "INVST_RES_LNDRD_NME"; // Landlord
																		// Name
	public static String INVST_RES_LNDRD_ADRS = "INVST_RES_LNDRD_ADRS"; // Landlord
																		// Address
	public static String INVST_RES_PAN_DPCTN = "INVST_RES_PAN_DPCTN"; // Pan
																		// Duplication
	public static String INVST_RES_FNAME = "INVST_RES_FNAME"; // Fname
	public static String INVST_RES_FUNQID = "INVST_RES_FUNQID"; // Funiqueid
	public static String INVST_RES_IS_SUBMIT = "INVST_RES_IS_SUBMIT"; // isSubmit
	public static String INVST_RES_UPDT_PROP_MSG = "INVST_RES_UPDT_PROP_MSG";

	public static String INVST_RES_UPDT_MSG = "INVST_RES_UPDT_MSG";
	public static String INVST_RES_OMNI_FY = "INVST_RES_OMNI_FY"; // OmniFY
	public static String INVST_RES_IS_FREEZE = "INVST_RES_IS_FREEZE"; // IS_Freeze
	public static String INVST_RES_PTRL_ELGBL = "INVST_RES_PTRL_ELGBL"; // Petrol
																		// Eligible
	public static String INVST_RES_PAN = "INVST_RES_PAN"; // PAN

	// for lta upload
	public static String INVST_RES_LTA = "INVST_RES_LTA";// lta main list
	public static String INVST_RES_AT = "INVST_RES_AT";// At list
	public static String INVST_RES_RT = "INVST_RES_RT";// rt list
	public static String INVST_RES_RTO = "INVST_RES_RTO";// rto list

	// Investment Upload Request Parameters

	public static String INVST_REQ_UPDATE_FL_LST = "INVST_REQ_UPDATE_FL_LST";
	public static String INVST_REQ_INVST_CD = "INVST_REQ_INVST_CD";
	public static String INVST_REQ_FL_NM = "INVST_REQ_FL_NM";
	public static String INVST_REQ_UPDT_BY_ID = "INVST_REQ_UPDT_BY_ID";
	public static String INVST_REQ_CAT_ID = "INVST_REQ_CAT_ID";
	public static String INVST_REQ_UPDT_AMNT = "INVST_REQ_UPDT_AMNT";
	public static String INVST_REQ_T05_INV_ITEM = "INVST_REQ_T05_INV_ITEM";
	public static String INVST_REQ_ANUL_LTB_A = "INVST_REQ_ANUL_LTB_A";
	public static String INVST_REQ_MNCP_TX_PD_B = "INVST_REQ_MNCP_TX_PD_B";
	public static String INVST_REQ_STRD_DDCT_D = "INVST_REQ_STRD_DDCT_D";
	public static String INVST_REQ_INTRST_BRWD_CP_E = "INVST_REQ_INTRST_BRWD_CP_E";
	public static String INVST_REQ_PRP_ADDR = "INVST_REQ_PRP_ADDR";
	public static String INVST_REQ_LNDR_NM = "INVST_REQ_LNDR_NM";
	public static String INVST_REQ_LNDR_ADD = "INVST_REQ_LNDR_ADD";
	public static String INVST_REQ_LNDR_PAN = "INVST_REQ_LNDR_PAN";
	public static String INVST_REQ_LNDR_NO = "INVST_REQ_LNDR_NO";
	public static String INVST_REQ_PRPTY_TYP = "INVST_REQ_PRPTY_TYP";
	public static String INVST_REQ_UPDATE_PROP_LST = "INVST_REQ_UPDATE_PROP_LST";
	public static String INVST_REQ_CAT_TYP = "INVST_REQ_CAT_TYP";
	public static String INVST_REQ_PL_ORG = "INVST_REQ_PL_ORG";
	public static String INVST_REQ_DSTN = "INVST_REQ_DSTN";
	public static String INVST_REQ_FRM_DTE = "INVST_REQ_FRM_DTE";
	public static String INVST_REQ_TO_DTE = "INVST_REQ_TO_DTE";
	public static String INVST_REQ_CRTD_BY = "INVST_REQ_CRTD_BY";
	public static String INVST_REQ_UP_FL_NM = "INVST_REQ_UP_FL_NM";
	public static String INVST_REQ_IP_ADDRESS = "INVST_REQ_IP_ADDRESS";
	public static String INVST_REQ_IS_SUBMIT = "INVST_REQ_IS_SUBMIT";
	public static String INVST_REQ_PLACE = "INVST_REQ_PLACE";
	public static String INVST_REQ_DELETE = "INVST_REQ_DELETE";

	/*
	 * public static String INVST_REQ_LENDER_NM = "INVST_REQ_LENDER_NM";//lender
	 * name update public static String INVST_REQ_LENDER_ADD =
	 * "INVST_REQ_LENDER_ADD";//lender address update public static String
	 * INVST_REQ_LENDER_PAN = "INVST_REQ_LENDER_PAN";//lender pan update
	 */
	public static String INVST_REQ_UPDATE_LTA_LST = "INVST_REQ_UPDATE_LTA_LST";
	public static String INVST_REQ_UPDATE_SUMMARY_LST = "INVST_REQ_UPDATE_SUMMARY_LST";
	public static String INVST_REQ_UPDATE_LENDER_LST = "INVST_REQ_UPDATE_LENDER_LST";// lender
																						// update
																			// list
	
	public static String INVST_REQ_DELETE_LENDER_LST = "INVST_REQ_DELETE_LENDER_LST";// lender
																						// update
																						// list
	public static String INVST_DECL_REQ_LST_UPDT_EXCHANGE_STS = "INVST_DECL_REQ_LST_UPDT_EXCHANGE_STS";
	// public static String INVST_UPLD_REQ_EMP_ID = "INVST_UPLD_REQ_EMP_ID";
	// public static String INVST_UPLD_REQ_TX_CAL_CD =
	// "INVST_UPLD_REQ_TX_CAL_CD";
	// public static String INVST_UPLD_REQ_INVST_ITEM =
	// "INVST_UPLD_REQ_INVST_ITEM";
	// public static String INVST_UPLD_REQ_INVST_AMT =
	// "INVST_UPLD_REQ_INVST_AMT";
	// public static String INVST_UPLD_REQ_TYPE = "INVST_UPLD_REQ_TYPE";
	// public static String INVST_UPLD_REQ_FILE_NM = "INVST_UPLD_REQ_FILE_NM";

	// Investment Classes Objects Count
	public static String INVST_RSP_OTH_INV_FL_UP_OBJS = "INVST_RSP_OTH_INV_FL_UP_OBJS";
	public static String INVST_RSP_OTH_INV_CNT_OBJS = "INVST_RSP_OTH_INV_CNT_OBJS";
	public static String INVST_RSP_RNT_FL_UP_OBJS = "INVST_RSP_RNT_FL_UP_OBJS";
	public static String INVST_RSP_LRD_DTS_OBJS = "INVST_RSP_LRD_DTS_OBJS";
	public static String INVST_RSP_PAN_VDT_OBJS = "INVST_RSP_PAN_VDT_OBJS";

	// get employee details request param for omnidocs
	public static String EMP_DETS_REQ_EMP_ID = "EMP_DETS_REQ_EMP_ID";

	// get employee details response param for omnidocs

	// Talk2HR Request
	public static String TALK_2_HR_REQ_EMP_ID = "TALK_2_HR_REQ_EMP_ID";
	public static String TALK_2_HR_REQ_ANTHR = "TALK_2_HR_REQ_ANTHR";

	// talk to hr responce
	public static String TALK_2_HR_RES_MSG = "TALK_2_HR_RES_MSG";// talk to hr
	public static String TALK_2_HR_RES_FLAG = "TALK_2_HR_RES_FLAG";// talk to hr
	
	public static String SRV_NM_PND_CRD = "SRV_NM_PND_CRD"; //  Pending Card Services
	
	// Pending Card Services
	public static String PND_CRD_RSP_EMPID = "PND_CRD_RSP_EMPID";
	public static String PND_CRD_RSP_NAME = "PND_CRD_RSP_NAME";
	public static String PND_CRD_RSP_TYP_NM = "PND_CRD_RSP_TYP_NM";
	public static String PND_CRD_RSP_TRNSCTN_NBR = "PND_CRD_RSP_TRNSCTN_NBR";
	public static String PND_CRD_RSP_EMP_RCD = "PND_CRD_RSP_EMP_RCD";
	public static String PND_CRD_RSP_BGN_DT = "PND_CRD_RSP_BGN_DT"; ////////////////
	public static String PND_CRD_RSP_PIN_TAKE_NUM = "PND_CRD_RSP_PIN_TAKE_NUM";
	public static String PND_CRD_RSP_END_DT = "PND_CRD_RSP_END_DT"; ////////////////
	public static String PND_CRD_RSP_ORIG_EMPID = "PND_CRD_RSP_ORIG_EMPID";
	public static String PND_CRD_RSP_ORIG_EMP_RCD = "PND_CRD_RSP_ORIG_EMP_RCD";
	public static String PND_CRD_RSP_ORIG_BEGIN_DT = "PND_CRD_RSP_ORIG_BEGIN_DT"; ////////////////
	public static String PND_CRD_RSP_ORIG_PIN_TAKE_NUM = "PND_CRD_RSP_ORIG_PIN_TAKE_NUM";
	public static String PND_CRD_RSP_ORIG_END_DT = "PND_CRD_RSP_ORIG_END_DT"; ////////////////
	public static String PND_CRD_RSP_ELIG_GRP = "PND_CRD_RSP_ELIG_GRP";
	public static String PND_CRD_RSP_JOBTITLE = "PND_CRD_RSP_JOBTITLE";
	public static String PND_CRD_RSP_DESCR = "PND_CRD_RSP_DESCR";
	public static String PND_CRD_RSP_WF_STS = "PND_CRD_RSP_WF_STS";
	public static String PND_CRD_RSP_DRTN_ABS = "PND_CRD_RSP_DRTN_ABS";
	public static String PND_CRD_RSP_ACTION_DT_SS = "PND_CRD_RSP_ACTION_DT_SS"; ////////////////
	public static String PND_CRD_RSP_AWSTEP_STS = "PND_CRD_RSP_AWSTEP_STS";
	public static String PND_CRD_RSP_OPRID = "PND_CRD_RSP_OPRID";
	public static String PND_CRD_RSP_SPRVSR_ID = "PND_CRD_RSP_SPRVSR_ID";
	public static String PND_CRD_RSP_DATE_TRANS_C = "PND_CRD_RSP_DATE_TRANS_C"; 
	public static String PND_CRD_RSP_PUNCH_TIME = "PND_CRD_RSP_PUNCH_TIME"; ////////////////
	public static String PND_CRD_RSP_PUNCH_END_TIME = "PND_CRD_RSP_PUNCH_END_TIME"; ////////////////
	public static String PND_CRD_RSP_TIMEZONE = "PND_CRD_RSP_TIMEZONE";
	public static String PND_CRD_RSP_TRC = "PND_CRD_RSP_TRC";
	public static String PND_CRD_RSP_TL_QNTY = "PND_CRD_RSP_TL_QNTY";
	public static String PND_CRD_RSP_REPORTED_STS = "PND_CRD_RSP_REPORTED_STS";
	public static String PND_CRD_RSP_PUNCH_TIME_2 = "PND_CRD_RSP_PUNCH_TIME_2";
	public static String PND_CRD_RSP_RMRKS_I20 = "PND_CRD_RSP_RMRKS_I20";

	// EGES Service Parameters 
	public static String SRV_NM_EGES_JOB_LIST = "SRV_NM_EGES_JOB_LIST"; //  Pending job list Services
	public static String SRV_NM_EGES_JOB_DESC = "SRV_NM_EGES_JOB_DESC"; //  Pending Job Description Services
	public static String SRV_NM_EGES_UPD_RESUME = "SRV_NM_EGES_UPD_RESUME"; //  Upload Resume  Services
	public static String SRV_NM_EGES_SMT_RESUME = "SRV_NM_EGES_SMT_RESUME"; //  Final Submit Resume With Details Services
	
	
	

	
	// Request parameters 
	
	public static String EGES_REQ_EMPID = "EGES_REQ_EMPID";
	public static String EGES_REQ_NUMBER = "EGES_REQ_NUMBER";
	public static String EGES_REQ_TITLE = "EGES_REQ_TITLE";
	public static String EGES_REQ_INTENT_ID = "EGES_REQ_INTENT_ID";
	public static String EGES_REQ_FILE_NAME = "EGES_REQ_FILE_NAME";
	public static String EGES_REQ_RESUME_DOC = "EGES_REQ_RESUME_DOC";
	public static String EGES_REQ_TYPE = "EGES_REQ_TYPE";
	public static String EGES_REQ_FIRST_NAME = "EGES_REQ_FIRST_NAME";
	public static String EGES_REQ_LAST_NAME = "EGES_REQ_LAST_NAME";
	public static String EGES_REQ_EMAIL_ID = "EGES_REQ_EMAIL_ID";
	public static String EGES_REQ_MOBILE_NO = "EGES_REQ_MOBILE_NO";
	public static String EGES_REQ_DOB = "EGES_REQ_DOB";
	public static String EGES_REQ_EMPMAILID = "EGES_REQ_EMPMAILID";
	 
	// EGES Response Parameters
	public static String EGES_RES_INDENT_ID = "EGES_RES_INDENT_ID";
	public static String EGES_RES_JOB_CODE = "EGES_RES_JOB_CODE";
	public static String EGES_RES_JOB_TITLE = "EGES_RES_JOB_TITLE";
	public static String EGES_RES_JOB_LOCATION = "EGES_RES_JOB_LOCATION";
	public static String EGES_RES_EXP_IN_YR = "EGES_RES_EXP_IN_YR";
	public static String EGES_RES_JOB_SKILLS = "EGES_RES_JOB_SKILLS";
	public static String EGES_RES_STATUS = "EGES_RES_STATUS";
	public static String EGES_RES_TITLE = "EGES_RES_TITLE";
	public static String EGES_RES_SUB_GROUP = "EGES_RES_SUB_GROUP";
	public static String EGES_RES_EDUCATION = "EGES_RES_EDUCATION";
	public static String EGES_RES_MANDTRY_SKILLS = "EGES_RES_MANDTRY_SKILLS";
	public static String EGES_RES_INTENT_DESC = "EGES_RES_INTENT_DESC";
	public static String EGES_RES_FIRST_NAME = "EGES_RES_FIRST_NAME";
	public static String EGES_RES_LAST_NAME = "EGES_RES_LAST_NAME";
	public static String EGES_RES_EMAIL_ID = "EGES_RES_EMAIL_ID";
	public static String EGES_RES_MOBILE_NO = "EGES_RES_MOBILE_NO";
	public static String EGES_RES_DOB = "EGES_RES_DOB";
	public static String EGES_RES_CONDT_ID = "EGES_RES_CONDT_ID";
	public static String EGES_RES_NUMBER = "EGES_RES_NUMBER";

		public static String  NEES_PENDING_REQ_LIST= "NEES_PENDING_REQ_LIST_MULTI*";
		public static String  NEES_APPR_OR_REJECT_REQ = "NEES_APPR_OR_REJECT_REQ_MULTI*";
		
		//Expense start 
		public static final String COMN_LIST = "COMN_LIST_MULTI*_JUSTHIT*";
		public static final String  TRAVEL_LIST= "TRAVEL_LIST_MULTI*_JUSTHIT*";
		public static final String  TRAVEL_DTL= "TRAVEL_DTL_MULTI*_JUSTHIT*";
		public static final String  TRAVEL_ACT = "TRAVEL_ACT_MULTI*_JUSTHIT*";
		public static final String R_TRAVEL_LIST = "R_TRAVEL_LIST_MULTI*_JUSTHIT*";
		public static final String R_TRAVEL_DTL = "R_TRAVEL_DTL_MULTI*_JUSTHIT*";
		public static final String R_TRAVEL_ACT = "R_TRAVEL_ACT_MULTI*_JUSTHIT*";
		public static final String TRAVEL_ACT_ALL = "TRAVEL_ACT_ALL_MULTI*_JUSTHIT*";
		public static final String R_TRAVEL_ACT_ALL = "R_TRAVEL_ACT_ALL_MULTI*_JUSTHIT*";
		public static final String TRAVEL_MULTI_ACT_ALL = "TRAVEL_MULTI_ACT_ALL_MULTI*_JUSTHIT*";
		public static final String  TRVL_APRVR= "TRVL_APRVR";
		public static final String  TRVL_RCMDR= "TRVL_RCMDR";
		public static final String  BE_APR= "BE_APR";
		public static final String  BE_REC= "BE_REC";
		public static final String  OE_APR= "OE_APR";
		public static final String  OE_REC= "OE_REC";
		public static final String NOT_SPEC = "NOT_SPEC";
		public static final String APR = "APR";
		public static final String REC = "REC";
		public static final String TE = "TE";
		public static final String BE = "BE";
		public static final String OE = "OE";
		public static final String A = "A";
		public static final String R = "R";
		public static final String APPROVAL = "Approval";
		public static final String RECOMMENDATION = "Recommendation";
		
		
		public static final String COMN_SRV_PROCEHIT = "COMN_SRV_MULTI*_PROCEHIT*";
		public static final String COMN_SRV_APIHIT = "COMN_APIHIT_MULTI*_JUSTHIT*";
		public static final String NA = "NA";
		//Expense end

	

		// Fusion leave Service
		public static String SRV_NM_FUS_LV_APY = "SRV_NM_FUS_LV_APY";
 
		// Fusion leave apply code
		public static String FUS_LV_AP_REQ_PRS_NO = "FUS_LV_AP_REQ_PRS_NO"; // Person Number - employee id
		public static String FUS_LV_AP_REQ_EMLR = "FUS_LV_AP_REQ_EMLR"; // employer 
		public static String FUS_LV_AP_REQ_ABS_TY = "FUS_LV_AP_REQ_ABS_TY"; // Absence Type
		public static String FUS_LV_AP_REQ_ST_DT_DUR = "FUS_LV_AP_REQ_ST_DT_DUR"; // Start Date Duration
		public static String FUS_LV_AP_REQ_ST_DT = "FUS_LV_AP_REQ_ST_DT"; // Start Date
		public static String FUS_LV_AP_REQ_ST_TM = "FUS_LV_AP_REQ_ST_TM"; // Start Time
		public static String FUS_LV_AP_REQ_END_DT = "FUS_LV_AP_REQ_END_DT"; // End Date
		public static String FUS_LV_AP_REQ_END_TM = "FUS_LV_AP_REQ_END_TM"; // End Time
		public static String FUS_LV_AP_REQ_ABS_STS_CD = "FUS_LV_AP_REQ_ABS_STS_CD"; // Absence Status Code

		
		//public static final String  OFFICE_LIST= "OFFICE_LIST_MULTI*_JUSTHIT*";
		public static final String  OFFICE_DTL= "OFFICE_DTL_MULTI*_JUSTHIT*";
		public static final String  OFFICE_ACT = "OFFICE_ACT_MULTI*_JUSTHIT*";
		//public static final String R_OFFICE_LIST = "R_TRAVEL_LIST_MULTI*_JUSTHIT*";
		public static final String R_OFFICE_DTL = "R_OFFICE_DTL_MULTI*_JUSTHIT*";
		public static final String R_OFFICE_ACT = "R_OFFICE_ACT_MULTI*_JUSTHIT*";
		public static final String OFFICE_ACT_ALL = "OFFICE_ACT_ALL_MULTI*_JUSTHIT*";
		
		//public static final String  BRANCH_LIST= "TRAVEL_LIST_MULTI*_JUSTHIT*";
		public static final String  BRANCH_DTL= "BRANCH_DTL_MULTI*_JUSTHIT*";
		public static final String  BRANCH_ACT = "BRANCH_ACT_MULTI*_JUSTHIT*";
		//public static final String R_BRANCH_LIST = "R_TRAVEL_LIST_MULTI*_JUSTHIT*";
		public static final String R_BRANCH_DTL = "R_BRANCH_DTL_MULTI*_JUSTHIT*";
		public static final String R_BRANCH_ACT = "R_BRANCH_ACT_MULTI*_JUSTHIT*";
		public static final String BRANCH_ACT_ALL = "BRANCH_ACT_ALL_MULTI*_JUSTHIT*";

		public static final String OLD_COUNT = "OLD_COUNT_MULTI*_";
		public static final String OLD_COUNT_FLAG = "OLD_COUNT";
		public static final String TRVL_N_OTHR_COUNT = "TRVL_N_OTHR_COUNT";
		public static final String NEES_COUNT = "NEES_COUNT";
		public static final String EXP_APP = "EXP_APP";
		public static final String LV_APP = "LV_APP";
		public static final String MT_APP = "MT_APP";
		public static final String LP_APP = "LP_APP";
		public static final String EXP_TRVL_APP = "EXP_TRVL_APP";
		public static final String EXP_OFC_APP = "EXP_OFC_APP";
		public static final String EXP_BRN_APP = "EXP_BRN_APP";
		public static final String NEES = "NEES";
		public static final String TRAINING_LIST = "TRAINING_LIST_MULTI*";
		public static final String TRAINING_ACT = "TRAINING_ACT_MULTI*";
		public static final String TE_TITL = "Travel";
		public static final String BE_TITL = "Branch";
		public static final String OE_TITL = "Other";
		public static final String EXP_TITL = "Expense";
		public static final String LV_TITL = "Leave";
		public static final String MT_TITL = "Muster";
		public static final String LP_TITL = "LatePunch";

//------- expense multiple approval start --------------------------
	public static String ADT_CD_EXP_MUL_APR = "EXP_MUL_APR";
	public static String SRV_NM_EXP_MUL_APP_REJ = "SRV_NM_EXP_MUL_APP_REJ"; 
	
//	------- expense multiple approval end --------------------------
	
	
//--------- lead creation start------------------------------------------
	public static String SRV_NM_LD_CRTN_SUBMIT = "SRV_NM_LD_CRTN_SUBMIT";
	public static String SRV_NM_LD_CRTN_GET_DTL_LST = "SRV_NM_LD_CRTN_GET_DTL_LST";
	 public static String SRV_NM_LD_CRTN_GET_BY_BRANCH = "SRV_NM_LD_CRTN_GET_BY_BRANCH";
	    public static String SRV_NM_TRVL_DTLS = "SRV_NM_TRVL_DTLS";
	    public static String TRVL_DTLS_REQ_TYPE = "TRVL_DTLS_REQ_TYPE";
	    public static String TRVL_DTLS_EMAIL_ID = "TRVL_DTLS_EMAIL_ID";
	    public static String TRVL_DTLS_PACKAGE_ID = "TRVL_DTLS_PACKAGE_ID";
	    public static String TRVL_RES_DATA = "TRVL_RES_DATA";
	    public static String SRV_NM_E_PROCUREMENT_ASSESTDETAIL = "SRV_NM_E_PROCUREMENT_ASSESTDETAIL";
	    public static String SRV_NM_E_PROCUREMENT_ASSESTDETAILACK = "SRV_NM_E_PROCUREMENT_ASSESTDETAILACK";
	    public static String SRV_NM_E_PROCUREMENT_PENDINGCOUNT = "SRV_NM_E_PROCUREMENT_PENDINGCOUNT";
	    public static String SRV_NM_E_PROCUREMENT_MAPPEDASSETDETAIL = "SRV_NM_E_PROCUREMENT_MAPPEDASSETDETAIL";
	    public static String E_LOCATION_NEW = "E_LOCATION_NEW";
	    public static String E_LOCATION_OLD = "E_LOCATION_OLD";
	    public static String E_ASSEST_CODE = "E_ASSEST_CODE";
	    public static String E_ASSEST_SERIAL_NO = "E_ASSEST_SERIAL_NO";
	    public static String E_SUBCATEGORY = "E_SUBCATEGORY";
	    public static String E_MAKEMODEL = "E_MAKEMODEL";
	    public static String E_MAKER = "E_MAKER";
	    public static String E_ASSIGNEE = "E_ASSIGNEE";
	    public static String E_OWNBEHOF = "E_OWNBEHOF";
	    public static String E_LOCATIONOLD1 = "E_LOCATIONOLD1";
	    public static String E_LOCATIONNEW1 = "E_LOCATIONNEW1";
	    public static String E_RANKING = "E_RANKING";
	    public static String E_TOBEMAPCHECKERID = "E_TOBEMAPCHECKERID";
	    public static String E_APPCHECKERID = "E_APPCHECKERID";
	    public static String E_LOCATIONADD = "E_LOCATIONADD";
	    public static String E_MAKERID = "E_MAKERID";
	    public static String E_MAKERNAME = "E_MAKERNAME";
	    public static String E_MAKERIDNAME = "E_MAKERIDNAME";
	    public static String E_OWNBEHOFID = "E_OWNBEHHAFID";
	    public static String E_OWNBEHOFNAME = "E_OWNBEHOFNAME";
	    public static String E_APPSTATUS = "E_APPSTATUS";
	    public static String E_ASSESTREJSTS = "E_ASSESTREJSTS";
	    public static String E_TOBESTATUS = "E_TOBESTATUS";
	    public static String E_LOCATIONADD1 = "E_LOCATIONADD1";
	    public static String E_STATUSOFASSEST = "E_STATUSOFASSEST";
	    public static String E_ID = "E_ID";
	    public static String E_PENDING_LAPTOPCOUNT = "E_PENDING_LAPTOPCOUNT";
	    public static String E_ASSEST_ACKCOUNT = "E_ASSEST_ACKCOUNT";
	    public static String E_ASSET_PRODUCTNAME = "E_ASSET_PRODUCTNAME";
	    public static String E_ASSET_NEGOTIATEDPRICE = "E_ASSET_NEGOTIATEDPRICE";
	    public static String E_ASSET_MAKERDATE = "E_ASSET_MAKERDATE";
	    public static String E_ASSET_LOCATIONCODE = "E_ASSET_LOCATIONCODE";
	    public static String E_OWNBEHOFIDNAME = "E_ASSET_OWNBEHOFIDNAME";
	
//--------- lead creation end------------------------------------------	




}
 


 


package com.engage.service;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.engage.bean.UserInfoBean;
import com.engage.bean.cache.CacheProvider;
import com.engage.broker.service.IntegrationBrokerService;
import com.engage.commons.IBConstants;
import com.engage.commons.UOTMFrameworkConstants.ImageUploadConstants;
import com.engage.commons.UOTMFrameworkConstants.ResponseCode;
import com.engage.commons.UotmConfiguration;
import com.engage.framework.exception.ErrorMessageProvider;
import com.engage.payload.vo.tfeedback.Tfeedback.Error;
import com.engage.payload.vo.tfeedback.Tfeedback.TFeedbackRequest;
import com.engage.payload.vo.tfeedback.Tfeedback.TFeedbackResponse;
import com.engage.security.AESAlgoClass;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.googlecode.protobuf.format.JsonFormat;
@Service
public class TFeedbackServiceImpl implements TFeedbackService {
	private static final Logger log = LogManager.getLogger(TFeedbackServiceImpl.class);
	private String class_name = "TFeedbackServiceImpl: ";
	@Autowired
	MyTeamManagerService myTeamManagerService;

//	@Autowired
//	CoreHelper coreHelper;
	@Autowired
	CacheProvider cacheProvider;
	@Autowired
	private ErrorMessageProvider errorMessageProvider;
	@Autowired
	private UotmConfiguration<String, HashMap<String, String>> applicationConfig;
	/*
	 * @Autowired private UserInfoBean userInfoBean;
	 */
	@Autowired
	private IntegrationBrokerService integrationBrokerService;

	// TODO set them in constants
	public static final SimpleDateFormat sdfYear = new SimpleDateFormat("yyyy");
	
	
	

	private String getImageName(HashMap<String, Object> imageReqDataMap) {
		StringBuilder imageNameBuilder = new StringBuilder();
		// imageNameBuilder.append((String)
		// applicationConfig.getValue(ImageUploadConstants.IMAGE_CONFIG)
		// .get(ImageUploadConstants.IMG_PREFIX));
		// imageNameBuilder.append(ImageUploadConstants.VALUE_SPLITTER);
		imageNameBuilder.append(imageReqDataMap.get(ImageUploadConstants.EMPLOYEE_ID));
		imageNameBuilder.append(ImageUploadConstants.PROFILE_IMAGE_FORMAT);

		return imageNameBuilder.toString();
	}
	
	
	
	private String generateProfileImgUrlEnc(String profileImageName) throws Exception {
		log.info("In " + this.getClass().getCanonicalName() + "/generateProfileImgUrlEnc()");

		String entityType = ImageUploadConstants.IMAGE_ENTITY_TYPE;

		StringBuilder profileImgUrlParam = new StringBuilder();

		profileImgUrlParam.append(ImageUploadConstants.ENTITY);
		profileImgUrlParam.append(ImageUploadConstants.VALUE_ASSIGNER);
		profileImgUrlParam.append(entityType);
		profileImgUrlParam.append(ImageUploadConstants.URL_APPENDER);
		profileImgUrlParam.append(ImageUploadConstants.IMAGE);
		profileImgUrlParam.append(ImageUploadConstants.VALUE_ASSIGNER);
		profileImgUrlParam.append(profileImageName);

		String encParam = AESAlgoClass.encrypt(profileImgUrlParam.toString());

		StringBuilder profileImgUrl = new StringBuilder();

		profileImgUrl.append((String) applicationConfig.getValue(ImageUploadConstants.IMAGE_CONFIG)
				.get(ImageUploadConstants.IMAGE_BASE_URL_ENC));
		profileImgUrl.append(ImageUploadConstants.ENCRIPTION);
		profileImgUrl.append(ImageUploadConstants.VALUE_ASSIGNER);
		profileImgUrl.append(URLEncoder.encode(encParam, "UTF-8"));

		log.info("Profile image url is : " + profileImgUrl.toString());

		return profileImgUrl.toString();
	}
	

	public static Map<String, Object> toMap(JSONObject jsonobj) throws JSONException {
		Map<String, Object> map = new HashMap<String, Object>();
		Iterator<String> keys = jsonobj.keys();
		while (keys.hasNext()) {
			String key = keys.next();
			Object value = jsonobj.get(key);
			if (value instanceof JSONArray) {
				value = toList((JSONArray) value);
			} else if (value instanceof JSONObject) {
				value = toMap((JSONObject) value);
			}
			map.put(key, value);
		}
		return map;
	}

	public static List<Object> toList(JSONArray array) throws JSONException {
		List<Object> list = new ArrayList<Object>();
		for (int i = 0; i < array.length(); i++) {
			Object value = array.get(i);
			if (value instanceof JSONArray) {
				value = toList((JSONArray) value);
			} else if (value instanceof JSONObject) {
				value = toMap((JSONObject) value);
			}
			list.add(value);
		}
		return list;
	}

	private Error.Builder BuildError(String code, String msg, String desc, String reso) {
		log.info("TFeedbackServiceImpl: BuildError():");
		log.info("TFeedbackServiceImpl: BuildError():code : " + code);
		log.info("TFeedbackServiceImpl: BuildError():msg : " + msg);
		log.info("TFeedbackServiceImpl: BuildError():desc : " + desc);
		log.info("TFeedbackServiceImpl: BuildError():reso : " + reso);
		Error.Builder error = Error.newBuilder();
		error.setCode(code);
		error.setMsg(msg);
		error.setDesc(desc);
		error.setReso(reso);

		JsonFormat jsonFormat = new JsonFormat();

		return error;

	}

	@Override
	public String getRateeList(TFeedbackRequest.Builder reqBuilder, TFeedbackResponse.Builder resBuilder,
			HashMap<String, String> auditDataMap, String jwt, UserInfoBean userInfoBean, HttpServletRequest request) {
		log.info("getRateeList / serive call() : ");
		String method_name = "getRateeList(): ";
		ObjectMapper mapper = new ObjectMapper();
		JsonFormat jsonFormat = new JsonFormat();
		JSONObject jsonObject = null;

		List<HashMap<String, Object>> srvDataList = null;


		HashMap<String, Object> integrationBorkerMap = null;
		HashMap<String, Object> integrationBrokerResponseMap = null;
		log.info("in " + this.getClass().getName() + " TFeedbackServiceImpl");

		try {

			String empId = userInfoBean.getEmpId();

			log.info("empId " + empId);

			String data = reqBuilder.getData();
			log.info("data " + data);

			try {
				jsonObject = new JSONObject(data);
				log.info("jsonObject " + jsonObject);
			} catch (JSONException err) {
				log.info("JSONException " + this.getClass().getName() + " TFeedbackServiceImpl");

				return jsonFormat.printToString(resBuilder.setError(
						BuildError(ResponseCode._500, "Server Error", "exception in jsonParsing call", "").build())
						.build());

			}
			String serviceName = jsonObject.getString("ServiceName");
			log.info("serviceName " + serviceName);

			Map<String, Object> hmReq = TFeedbackServiceImpl.toMap(jsonObject);
			log.info("hmReq " + hmReq);

			// HashMap<String, String> hmReq = new HashMap();
		
			//log.info("SRV_UID " + userInfoBean.getLoginId());
			// hmReq.put(IBConstants.T_RATER_ID,empId);
			integrationBorkerMap = new HashMap<String, Object>();
			integrationBorkerMap.put("SRV_UID", empId);
			integrationBorkerMap.put("SRV_NM", IBConstants.SRV_NM_RATEE_LIST);

			integrationBorkerMap.put("AUDIT_DATA", auditDataMap);
			integrationBorkerMap.put("SRV_DATA", hmReq);
			log.info("SRV_DATA ");

	
			integrationBrokerResponseMap = new HashMap<String, Object>();

		//	integrationBrokerResponseMap = integrationBrokerService.callBrokerService(integrationBorkerMap);
			//log.info("integrationBrokerResponseMap" + integrationBrokerResponseMap);

			try {
				integrationBrokerResponseMap = integrationBrokerService.callBrokerService(integrationBorkerMap);
				log.info("TFeedbackServiceImpl: submit(): integrationBrokerResponseMap" + integrationBrokerResponseMap);
			} catch (Exception e) {

				e.printStackTrace();
				StringWriter sw = new StringWriter();
				e.printStackTrace(new PrintWriter(sw));

				log.info("Error occured in TFeedbackServiceImpl: submit() : " + sw.toString());
				log.error("Error occured in TFeedbackServiceImpl: submit() : " + sw.toString());
				return jsonFormat.printToString(resBuilder.setError(
						BuildError(ResponseCode._500, "Server Error", "exception in integration call", "").build())
						.build());
			}

			if (integrationBrokerResponseMap != null) {

				if (integrationBrokerResponseMap.get(IBConstants.SRV_RSP_STS) == null || !integrationBrokerResponseMap
						.get(IBConstants.SRV_RSP_STS).toString().equals(IBConstants._200)) {

					log.info("Error occured in TFeedbackServiceImpl: submit() : SRV_RSP_STS!=200");

					return jsonFormat.printToString(resBuilder.setError(BuildError(ResponseCode._500,
							integrationBrokerResponseMap.get(IBConstants.SRV_RSP_EXCPT).toString(), "Server Error", "")
									.build())
							.build());
				}
			} else {
				log.info("LeadCreationServiceImpl: submit() : integrationBrokerResponseMap is null");
				return jsonFormat.printToString(resBuilder.setError(
						BuildError(ResponseCode._500, "Server Error", "integrationBrokerResponseMap is null", "")
								.build())
						.build());
			}

			HashMap<String, Object> object = (HashMap<String, Object>) integrationBrokerResponseMap
					.get(IBConstants.SRV_RSP_DATA);

			if (object != null && !object.isEmpty()) {

				log.info("ratee data iterate  started" + new Date());
				List<HashMap<String, Object>> hashMaps = (List<HashMap<String, Object>>) object.get("data_list");
				srvDataList = new ArrayList<HashMap<String, Object>>();

				if (!hashMaps.isEmpty() && serviceName.equalsIgnoreCase("pendingFeedback")) {
					log.info("ratee hashMaps");

					for (HashMap<String, Object> hashMap : hashMaps) {
						 String ratteId = hashMap.get(IBConstants.T_RATEE_ID).toString();
						
						HashMap<String, Object> imageReqDataMap = new HashMap<String, Object>();
						imageReqDataMap.put(ImageUploadConstants.EMPLOYEE_ID,ratteId);

						String imageName = getImageName(imageReqDataMap);
						String strURL = generateProfileImgUrlEnc(imageName);
						log.info("strURL"+strURL);

					//	myTeamData.setUrl(strURL);

						HashMap<String, Object> srvFinalData = new HashMap<String, Object>();
						
						srvFinalData.put("ratteDepartment", hashMap.get(IBConstants.T_USER_DEPARTMENT).toString());
						
						srvFinalData.put("ratteName", hashMap.get(IBConstants.T_RATEE_NAME).toString());
						srvFinalData.put("cycleStartDate", hashMap.get(IBConstants.T_CYCLE_START_DATE).toString());
						srvFinalData.put("cycleEndDate", hashMap.get(IBConstants.T_CYCLE_END_DATE).toString());
						srvFinalData.put("feedbackCount", hashMap.get(IBConstants.T_FEEDBACK_COUNT).toString());
						
						srvFinalData.put("feedbackStatus", hashMap.get(IBConstants.T_FEEDBACK_STATUS).toString());
						srvFinalData.put("roleName", hashMap.get(IBConstants.T_ROLE_NAME).toString());
						
						
						srvFinalData.put("ratteId", hashMap.get(IBConstants.T_RATEE_ID).toString());
						srvFinalData.put("raterId", hashMap.get(IBConstants.T_RATER_ID).toString());
						srvFinalData.put("raterName", hashMap.get(IBConstants.T_RATER_NAME).toString());
						srvFinalData.put("cycleDesc", hashMap.get(IBConstants.T_CYCLE_DESC).toString());

						srvFinalData.put("cycleId", hashMap.get(IBConstants.T_CYCLE_ID).toString());
						srvFinalData.put("cycleName", hashMap.get(IBConstants.T_CYCLE_NAME).toString());
						srvFinalData.put("url", strURL);
						
						log.info("srvFinalData "+srvFinalData);
						srvDataList.add(srvFinalData);
						
						

					}
					String writeValueAsString = mapper.writeValueAsString(srvDataList);
					log.info("writeValueAsString "+writeValueAsString);

					String replace = writeValueAsString.replaceAll("\\\\", "");
					log.info("replace "+replace);
					resBuilder.setData(replace);
				}

				else if (!hashMaps.isEmpty() && serviceName.equalsIgnoreCase("getPendingFeedbackQuestionare")) {

					for (HashMap<String, Object> hashMap : hashMaps) {

						HashMap<String, Object> srvFinalData = new HashMap<String, Object>();
						
						
						srvFinalData.put("cycleSeqId", hashMap.get(IBConstants.T_CYCLE_SEQ_ID).toString());
						srvFinalData.put("questionId", hashMap.get(IBConstants.T_QUESTION_ID).toString());
						srvFinalData.put("competencyId", hashMap.get(IBConstants.T_COMPETENCY_ID).toString());
						srvFinalData.put("questionaireId", hashMap.get(IBConstants.T_QSTNARE_ID).toString());
						srvFinalData.put("questionText", hashMap.get(IBConstants.T_QSTN_TEXT).toString());

						srvFinalData.put("questionType", hashMap.get(IBConstants.T_QSTN_TYPE).toString());
						srvFinalData.put("qstnImptScaleId", hashMap.get(IBConstants.T_QSTN_IMPT_SCALE_ID).toString());

						srvFinalData.put("qstnFreqScaleId", hashMap.get(IBConstants.T_QSTN_FREQ_SCALE_ID).toString());
						srvFinalData.put("qstnExptScaleId", hashMap.get(IBConstants.T_QSTN_EXPT_SCALE_ID).toString());
						srvFinalData.put("questionOrder", hashMap.get(IBConstants.T_QSTN_ORDER).toString());
						srvFinalData.put("questionaireName", hashMap.get(IBConstants.T_QSTNARE_NAME).toString());

						srvFinalData.put("questionaireDesc", hashMap.get(IBConstants.T_QSTNARE_DESC).toString());
						srvFinalData.put("freqScale", hashMap.get(IBConstants.T_FREQ_SCALE).toString());

						srvFinalData.put("imptScale", hashMap.get(IBConstants.T_IMPT_SCALE).toString());
						
						srvFinalData.put("empList", hashMap.get(IBConstants.T_EMP_LIST).toString());
						log.info("srvFinalData "+srvFinalData);
						srvDataList.add(srvFinalData);

					}
					String writeValueAsString = mapper.writeValueAsString(srvDataList);

					String replace = writeValueAsString.replaceAll("\\\\", "");
					log.info("writeValueAsString "+writeValueAsString);

					log.info("replace "+replace);
					resBuilder.setData(replace);

				}

				else if (!hashMaps.isEmpty() && serviceName.equalsIgnoreCase("count")) {

					for (HashMap<String, Object> hashMap : hashMaps) {
						HashMap<String, Object> srvFinalData = new HashMap<String, Object>();
						srvFinalData.put("ratteCount", hashMap.get(IBConstants.T_RATEE_COUNT).toString());
						log.info("srvFinalData "+srvFinalData);
						srvDataList.add(srvFinalData);
					}
					String writeValueAsString = mapper.writeValueAsString(srvDataList);

					String replace = writeValueAsString.replaceAll("\\\\", "");
					log.info("writeValueAsString "+writeValueAsString);

					log.info("replace "+replace);
					resBuilder.setData(replace);

				}
				else if (!hashMaps.isEmpty() && serviceName.equalsIgnoreCase("saveFeedbackQuestionare")) {

					for (HashMap<String, Object> hashMap : hashMaps) {
						HashMap<String, Object> srvFinalData = new HashMap<String, Object>();
						srvFinalData.put("statusCode", hashMap.get(IBConstants.T_FDBK_STS_CODE).toString());
						srvFinalData.put("statusMes", hashMap.get(IBConstants.T_FDBK_STS_MSG).toString());
						log.info("srvFinalData "+srvFinalData);
						srvDataList.add(srvFinalData);
					}
					String writeValueAsString = mapper.writeValueAsString(srvDataList);

					String replace = writeValueAsString.replaceAll("\\\\", "");
					log.info("writeValueAsString "+writeValueAsString);

					log.info("replace "+replace);
					resBuilder.setData(replace);

				}
				
				

				else {
					log.info(class_name + method_name + "ratte list returned is nothing");
					return jsonFormat.printToString(resBuilder.setError(
							BuildError(ResponseCode._511, "No data available", "ratte list returned is nothing", "")
									.build())
							.build());
				}

			}

			else {
				log.info("LeadCreationServiceImpl: submit() : integrationBrokerResponseMap is null");
				return jsonFormat.printToString(resBuilder.setError(
						BuildError(ResponseCode._500, "Server Error", "integrationBrokerResponseMap is null", "")
								.build())
						.build());

			}

		} catch (Exception e) {
			log.error("Exception>>IB connection error" + "message:" + e.getMessage() + "\n cause:" + e.getCause());

			e.printStackTrace();
			StringWriter sw = new StringWriter();
			e.printStackTrace(new PrintWriter(sw));

			log.info(class_name + method_name + "Error occured: " + sw.toString());
			// log.error("Error occured in LeadCreationServiceImpl: submit()");

			return jsonFormat.printToString(resBuilder.setError(BuildError(ResponseCode._500, "Server Error",
					(String) applicationConfig.getValue(ResponseCode.PROPERTY_KEY).get(ResponseCode._500),
					e.getMessage()).build()).build());
		}

		return jsonFormat.printToString(resBuilder.build());

	}
	
	public static void main(String[] args) throws Exception {
		HashMap<String, Object> imageReqDataMap = new HashMap<String, Object>() ;
		imageReqDataMap.put(ImageUploadConstants.EMPLOYEE_ID,"231797");
		TFeedbackServiceImpl tFeedbackServiceImpl = new TFeedbackServiceImpl();
		String imageName = tFeedbackServiceImpl.getImageName(imageReqDataMap);
		String generateProfileImgUrlEnc = tFeedbackServiceImpl.generateProfileImgUrlEnc(imageName);
	
	}

}
